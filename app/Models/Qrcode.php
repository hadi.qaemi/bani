<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use App\Http\Resources\QrcodeResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Qrcode extends Model {

	protected $table = 'qrcodes';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['code', 'address_id'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function getQrcodes($request)
    {
        $qrcodes = $this->select(['*']);
        if (!empty($request->get('title'))) {
            $qrcodes->where('code',  $request->get('title'));
        }
        $qrcodes->get();
        $qrcodes->orderBy('id', 'desc');
        return QrcodeResource::collection($qrcodes->paginate($request->limit));
    }
}
