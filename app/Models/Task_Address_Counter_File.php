<?php

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;

class Task_Address_Counter_File extends Model {

	protected $table = 'task_has_address_has_counter_has_files';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'task_has_address_has_counter_id', 'file_path'];


}
