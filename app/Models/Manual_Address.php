<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Manual_Address extends Model {

	protected $table = 'manual_address';

    protected $guarded = [];
    protected $fillable = ['qrcode', 'powerMeter', 'address1'];

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

}
