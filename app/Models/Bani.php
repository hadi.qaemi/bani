<?php

namespace App\Models;

use App\Http\Resources\BaniResource;
use DB, Illuminate\Database\Eloquent\Model;

class Bani extends Model {

	protected $table = 'bellmans';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'shaba', 'card_id', 'account_number', 'validate', 'uid'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function user()
    {
        return $this->belongsTo('App\Laravue\Models\User', 'user_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Bani_Self_Address', 'bellman_id', 'id');
    }

    public function allBani($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
//        $addresses->whereNull('validate');
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return BaniResource::collection($addresses->paginate($request->limit));
    }

    public function autoCompleteBani($request)
    {
        $bani = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $bani->where('validate', 1);
        $bani->get();
        $bani->orderBy('id', 'desc');
        return BaniResource::collection($bani->paginate($request->limit));
    }
}
