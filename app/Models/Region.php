<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use App\Http\Resources\QrcodeResource;
use App\Http\Resources\RegionResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Region extends Model {

	protected $table = 'regions';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['id', 'region'];

    public function autoCompleteRegion($request)
    {
        $regions = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $regions->where('region', 'like', '%'.$request->get('q').'%');
        $regions->get();
        $regions->orderBy('id', 'desc');
        return RegionResource::collection($regions->paginate($request->limit));
    }

}
