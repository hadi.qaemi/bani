<?php

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Token_Wallet extends Model {

	protected $table = 'token_wallet';

    protected $guarded = [];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public function getIdAttribute($value)
    {
        return encrypt($value);
    }

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public function getStatusAttribute($value)
    {
        if($value == 0)
            return 'checking';
        if($value == 2)
            return 'deleted';
        return 'ok';
    }

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public static function addToken($request)
    {
        $wallet = self::create($request);
        return response()->json($wallet, 200);
    }

    /**
     * Get List of Customers
     *
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getWallets($request)
    {
        $wallets = $this->select(['*']);
        if (!empty($request->search['field'])) {
//            $searchField = $request->search['field'];
//            $searchValue = $request->search['value'];
//            $wallets->where($searchField, 'like', '%' . $searchValue . '%');
            $wallets->all();
            $wallets->orderBy('id', 'desc');
        }
        return $wallets->paginate($request->limit);
    }

	public function token()
    {
        return $this->belongsTo('App\Models\Token', 'token_id', 'id');
    }

	public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet', 'wallet_id', 'id');
    }

	public function user()
    {
        return $this->belongsTo('App\User', 'uid', 'id');
    }
}
