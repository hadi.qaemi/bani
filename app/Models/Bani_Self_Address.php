<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Bani_Self_Address extends Model {

	protected $table = 'bellman_has_addresses';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['bellman_id', 'address','geolocation','bellman_id'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function bani()
    {
        return $this->belongsTo('App\Laravue\Models\Bani', 'bellman_id', 'id');
    }

}
