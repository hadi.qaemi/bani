<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB,
    Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Assignment extends Model {

    protected $table = 'assignments';
    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'task_id', 'bellman_id', 'last_status'];

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public function task() {
        return $this->belongsTo('App\Models\Task', 'task_id', 'id');
    }

    public function bani() {
        return $this->belongsTo('App\Models\Bani', 'bellman_id', 'id');
    }

    public function histories() {
        return $this->hasMany('App\Models\Assignment_History', 'assignment_id', 'id')->get();
    }

    public function lastHistory() {
        return $this->hasMany('App\Models\Assignment_History', 'assignment_id', 'id')->latest('id')->first();
    }

}
