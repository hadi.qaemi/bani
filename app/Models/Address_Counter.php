<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Address_Counter extends Model {

	protected $table = 'address_has_counters';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['address_id', 'qr_code', 'type', 'metadata', 'subscribtion_code'];

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public static function AddCounter($address_id, $qr_code, $counter_type, $meatdata, $subscribtion_code)
    {
        return self::updateOrCreate(
            ['address_id' => $address_id, 'metadata' => $meatdata],
            ['qr_code' => $qr_code, 'type' => $counter_type, 'subscribtion_code' => $subscribtion_code]
        );
    }

    public function address() {
        return $this->belongsTo('App\Models\Address', 'address_id', 'id');
    }


    public function getRowColumnAttribute()
    {
        $metadata = preg_split('/,/', $this->metadata);
        return ['row' => $metadata[0], 'column' => $metadata[1], ];
    }


}
