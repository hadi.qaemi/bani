<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class History_Description extends Model {

	protected $table = 'history_has_descriptions';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'assignment_has_history_id', 'description'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

}
