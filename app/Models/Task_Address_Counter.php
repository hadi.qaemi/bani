<?php

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;

class Task_Address_Counter extends Model {

	protected $table = 'task_has_address_has_counters';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['bani_id', 'task_has_address_id', 'addresses_has_counter_id', 'value', 'status'];

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function bani()
    {
        return $this->belongsTo('App\Models\Bani', 'bani_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Task_Address', 'task_has_address_id', 'id');
    }

    public function counter()
    {
        return $this->belongsTo('App\Models\Address_Counter', 'addresses_has_counter_id', 'id');
    }


    public function files() {
        return $this->hasMany('App\Models\Task_Address_Counter_File', 'task_has_address_has_counter_id', 'id');
    }

}
