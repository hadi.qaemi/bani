<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Bani_Address extends Model {

	protected $table = 'selected_bellman_service';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['bellman_id', 'user_has_addresses_id', 'associate_type', 'is_verified', 'verification_code'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function user()
    {
        return $this->belongsTo('App\Laravue\Models\User', 'bellman_id', 'id');
    }

    public function bellman()
    {
        return $this->belongsTo('App\Models\Bani', 'bellman_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'user_has_addresses_id', 'id');
    }

}
