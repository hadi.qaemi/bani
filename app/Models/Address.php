<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\BaniAddressResource;
use App\Http\Resources\CounterResource;
use App\Http\Resources\LogResource;
use App\Http\Resources\MyCounterResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Address extends Model {

	protected $table = 'user_has_addresses';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'province', 'city', 'address', 'region', 'counter', 'geo_place'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function user()
    {
        return $this->belongsTo('App\Laravue\Models\User', 'user_id', 'id');
    }

    public function bani()
    {
        return $this->hasMany('App\Models\Bani_Address', 'user_has_addresses_id', 'id');
    }

    public function counters()
    {
        return $this->hasMany('App\Models\Address_Counter', 'address_id', 'id');
    }

    public function region_fa()
    {
        return $this->belongsTo('App\Models\Region', 'region', 'id');
    }

    public function owner()
    {
         $owner= $this->hasOne('App\Models\Bani_Address', 'user_has_addresses_id', 'id')->where('bellman_id', auth()->id())
             ->where('associate_type','Owner');
         return $owner->user->name;
    }

    public function getMyAddress($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        if (trim($request->title) != '') {
            $addresses->where('address', 'like', '%'.trim($request->title).'%');
        }
        $addresses->where('user_id', '=', auth()->id());
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return AddressResource::collection($addresses->paginate($request->limit));
    }

    public function getAddress($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $addresses->whereNull('status');
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return AddressResource::collection($addresses->paginate($request->limit));
    }

    public function showAddress($id)
    {
        $address = $this->find($id);
        dd($id);
        return AddressResource::collection($address);
    }

    public function getCounters($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $addresses->whereNotIn('id',
            Bani_Address::where('bellman_id', auth()->id())->pluck('user_has_addresses_id')
        );
        $addresses->where('status', '=', 1);
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return AddressResource::collection($addresses->paginate($request->limit));
    }

    public function getMyCounters($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $addresses->whereIn('id',
            Bani_Address::where('bellman_id', auth()->id())->pluck('user_has_addresses_id')
        );
        $addresses->where('status', '=', 1);
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return MyCounterResource::collection($addresses->paginate($request->limit));
    }

    public function autoCompleteRegion($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $addresses->where('region', 'like', '%'.$request->get('q').'%');
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return MyCounterResource::collection($addresses->paginate($request->limit));
    }


    public function autoCompleteAddress($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }
        $addresses->where('address', 'like', '%'.$request->get('q').'%');
        $tt = $addresses->get();
        $addresses->orderBy('id', 'desc');
        return MyCounterResource::collection($addresses->paginate($request->limit));
    }

    public function tasks(){
        return $this->belongsToMany('App\Models\Task', 'task_has_addresses', 'address_id', 'task_id');
    }

    public function getBaniAddresess($request)
    {
        $addresses = $this->select(['*']);
        if (!empty($request->search['field'])) {

        }

        $addresses->whereExists(function ($query) {
            $query->select("*")
                ->from('selected_bellman_service')
                ->whereRaw('selected_bellman_service.user_has_addresses_id = user_has_addresses.id')
                ->where('bellman_id', '=', auth()->user()->bani->id);
            ;
        });
        $addresses->get();
        $addresses->orderBy('id', 'desc');
        return BaniAddressResource::collection($addresses->paginate($request->limit));
    }


    public function countersLog($request)
    {
        $data = Address_Counter::where('address_id', $this->id)
            ->leftJoin('task_has_addresses', 'address_has_counters.id', '=', 'task_has_addresses.address_id')
            ->leftJoin('tasks', 'tasks.id', '=', 'task_has_addresses.task_id')
            ->leftJoin('assignments', 'assignments.task_id', '=', 'tasks.id')
            ->where('assignments.last_status', 'done')
            ->groupBy('address_has_counters.id')
            ->select(['max(assignments.updated_at) as last_view', 'address_has_counters.id', 'address_has_counters.metadata as position'])
            ->get();
        return LogResource::collection($data->paginate($request->limit));


    }

    public function countersInfo($request)
    {
        $data = Address_Counter::where('address_id', $this->id)
            ->leftJoin('task_has_addresses', 'address_has_counters.id', '=', 'task_has_addresses.address_id')
            ->leftJoin('tasks', 'tasks.id', '=', 'task_has_addresses.task_id')
            ->leftJoin('assignments', 'assignments.task_id', '=', 'tasks.id')
            ->orderBy('assignments.id desc')
            //->groupBy('address_has_attribute.id')
            ->select(['assigments.id as assignment_id','assignments.last_status as status', 'address_has_counters.as id', 'address_has_counters.metadata as position'])
            ->toSql();
        $query= \Illuminate\Support\Facades\DB::select('select * from ('. $data. ') t Group By id');
            //->get();
        return LogResource::collection($data->paginate($request->limit));


    }
}
