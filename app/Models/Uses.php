<?php

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;

class Uses extends Model {

	protected $table = 'uses';

    protected $guarded = [];
    protected $fillable = ['number', 'uses', 'date'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public function getIdAttribute($value)
    {
        return encrypt($value);
    }

    public static function addWallet($request)
    {
        $wallet = self::create($request);
        return response()->json($wallet, 200);
    }

    /**
     * Get List of Customers
     *
     * @access  public
     * @param
     * @return  json(array)
     */

    public function getWallets($request)
    {
        $wallets = $this->select(['*']);
        if (!empty($request->search['field'])) {
//            $searchField = $request->search['field'];
//            $searchValue = $request->search['value'];
            $wallets->all();
            $wallets->orderBy('id', 'desc');
        }
        $wallets->where('uid', '=', auth()->id());
        return $wallets->paginate($request->limit);
    }

    public function tokens()
    {
        return $this->hasMany('App\Models\Token_Wallet', 'wallet_id', 'id');
    }
}
