<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB, Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Task_Address extends Model {

	protected $table = 'task_has_addresses';

    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'address_id', 'task_id'];
    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */

    public function address()
    {
        return $this->belongsTo('App\Models\Address', 'address_id', 'id');
    }

    public function counter()
    {
        return $this->belongsTo('App\Models\Address_Counter', 'address_has_counter_id', 'id');
    }

    public function task()
    {
        return $this->belongsTo('App\Models\Task', 'task_id', 'id');
    }

    public function resource()
    {
        return $this->hasMany('App\Models\Task_Address_Counter', 'task_has_address_id', 'id');
    }

}
