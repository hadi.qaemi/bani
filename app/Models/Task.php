<?php

namespace App\Models;

use DB, Illuminate\Database\Eloquent\Model;

class Task extends Model {

	protected $table = 'tasks';

    protected $guarded = [];
    protected $fillable = ['title', 'explain', 'date', 'deadline', 'service_id', 'user_id'];


    public function user()
    {
        return $this->belongsTo('App\Laravue\Models\User', 'user_id', 'id');
    }

    public function assignments()
    {
        return $this->hasMany('App\Models\Assignment', 'task_id', 'id');
    }

    public function address()
    {
        return $this->hasOne('App\Models\Task_Address', 'task_id', 'id');
    }
}
