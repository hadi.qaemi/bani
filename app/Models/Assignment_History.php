<?php

namespace App\Models;

use App\Http\Resources\AddressResource;
use App\Http\Resources\MyCounterResource;
use DB,
    Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Assignment_History extends Model {

    protected $table = 'assignment_has_history';
    protected $guarded = ['updated_at', 'created_at'];
    protected $fillable = ['user_id', 'assignment_id', 'status'];

    /**
     * Replace Field
     *
     * @access  public
     * @param
     * @return  string
     */
    public function assignment() {
        return $this->belongsTo('App\Models\Assignment', 'assignment_id', 'id');
    }

    public function files() {
        return $this->hasMany('App\Models\History_File', 'assignment_has_history_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Laravue\Models\User', 'user_id', 'id');
    }

    public function description() {
        return $this->hasOne('App\Models\History_Description', 'assignment_has_history_id', 'id');
    }



}
