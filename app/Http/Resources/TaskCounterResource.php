<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;

class TaskCounterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->address->task->address->address) ? $this->address->task->address->address->id : '',
            'counter' => isset($this->counter) ? $this->counter->subscribtion_code : '',
            'counter_type' => isset($this->counter) ? ($this->counter->type == 'counter_new' ? 'new' : 'old') : '',
            'energic_code' => isset($this->counter) ? ($this->counter->qr_code%100) : '',
            'qr_code' => isset($this->counter) ? ($this->counter->qr_code) : '',
            'request_id' => $this->address->task->id,
            'user' => isset($this->address->task->user->name) ? $this->address->task->user->name . ( trim($this->address->task->user->family) == '' ? '' : ' '.$this->address->task->user->family) : '',
            'assignment_id' => isset($this->id) ? $this->id : null,
            'created_by' => isset($this->address->task->user->name) ? $this->address->task->user->name . ( trim($this->address->task->user->family) == '' ? '' : ' '.$this->address->task->user->family) : '',
            'title' => $this->address->task->title,
            'date' => $this->address->task->date,//Jalalian::fromDateTime($this->address->task->date)->toString(),
            'deadline' => $this->address->task->deadline,
            'explain' => $this->address->task->explain,
            'address' => (isset($this->address->task->address->address->province) ? $this->address->task->address->address->province : '').' '.(isset($this->address->task->address->address->city) ? $this->address->task->address->address->city : '').' '.(isset($this->address->task->address->address->address) ? $this->address->task->address->address->address : ''),
//            'address' => (isset($this->address->task->address->address->province) ? $this->address->task->address->address->province : '').' '.(isset($this->address->task->address->address->city) ? $this->address->task->address->address->city : '').' '.(isset($this->address->task->address->address->region) ? $this->address->task->address->address->region : '').' '.(isset($this->address->task->address->address->address) ? $this->address->task->address->address->address : ''),
            'service_id' => $this->address->task->service_id,
        ];
    }
}
