<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BaniResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => (isset($this->user->name) ? $this->user->name : '').' '.(isset($this->user->family) ? $this->user->family : ''),
            'shaba' => $this->shaba,
            'card_id' => $this->card_id,
            'account_number' => $this->account_number,
            'status' => ( $this->validate == null ? 'pending' : ( $this->validate == 0 ? 'notValid' : 'valid' ) ),
        ];
    }
}
