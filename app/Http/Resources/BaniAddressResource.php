<?php

namespace App\Http\Resources;

use App\Models\Bani_Address;
use Illuminate\Http\Resources\Json\JsonResource;

class BaniAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'owner' => isset($this->user->name) ? $this->user->name . ' ' . $this->user->family : '',
            'province' => $this->province,
            'city' => $this->city,
            'address' => $this->address,
            'region' => isset($this->region_fa->region) ? $this->region_fa->region : '',
            'last_view' => $this->last_view,
            'status' => ( $this->status == null ? 'pending' : ( $this->status == 0 ? 'notValid' : 'valid' ) ),
        ];
    }
}
