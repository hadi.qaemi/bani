<?php

namespace App\Http\Resources;

use App\Models\History_Description;
use App\Models\History_File;
use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;

class AssignmentHistoryResource extends JsonResource {

    private $deadline;

    public function __construct($resource, $deadline = null) {
        $this->resource = $resource;
        $this->deadline = $deadline;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $output = [
            'id' => $this->id,
            'assignment_id' => $this->id,
            'user' => (isset($this->bani) ? $this->bani->user->name : '') . ' ' . (isset($this->bani->user->family) ? $this->bani->user->family : ''),
            'status' => $this->status,
            'file_path' => $this->files->pluck('file_path')->first(),
            'value' => $this->value ,
            'date' => \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y-m-d h:i:s', Jalalian::fromDateTime($this->created_at)->toString())->format('Y-m-d h:i:s'),
        ];
        if ($this->deadline !== null) {
            $output['deadline'] = $this->deadline;
        }
        return $output;
    }

}
