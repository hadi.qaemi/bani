<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CounterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'position' => $this->metadata,
            'row_column' => $this->row_column,
            'type' => $this->type,
            'counter_type' => $this->type == 'counter_old' ? 'old' : 'new',
            'qr_code' => $this->qr_code,
            'subscribtion_code' => $this->subscribtion_code,
            'status' => $this->status,
        ];
    }
}
