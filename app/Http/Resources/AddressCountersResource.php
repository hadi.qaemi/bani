<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressCountersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => (isset($this->address->user->name) ? $this->address->user->name : '') . ' '.(isset($this->address->user->family) ? $this->address->user->family : ''),
            'province' => $this->address->province,
            'city' => $this->address->city,
            'address' => $this->address->address,
            'region' => $this->address->region,
            'geo_place' => $this->address->geo_place,
            'last_view' => $this->address->last_view,
            'qr_code' => $this->qr_code,
            'counter' => $this->subscribtion_code,
            'metadata' => $this->metadata,
            'status' => ( $this->status == null ? 'pending' : ( $this->status == 0 ? 'notValid' : 'valid' ) ),
        ];
    }
}
