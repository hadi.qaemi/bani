<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;

class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'position' => isset($this->counter->metadata) ? $this->counter->metadata : '1,1',
            'counter' => isset($this->counter->subscribtion_code) ? $this->counter->subscribtion_code : '',
            'last_view' => \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y-m-d h:i:s', Jalalian::fromDateTime($this->created_at)->toString())->format('Y-m-d h:i:s')
        ];
    }
}
