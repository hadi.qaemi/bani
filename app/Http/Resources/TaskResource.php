<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Morilog\Jalali\Jalalian;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->address->address) ? $this->address->address->id : '',
            'counter' => isset($this->address->address) ? $this->address->address->counter : '',
            'counter_type' => isset($this->address->address) ? $this->address->address->counter_type : '',
            'request_id' => $this->id,
            'user' => $this->user->name . ' '.$this->user->family,
            'assignment_id' => isset($this->assignments[0]) ? $this->assignments[0]->id : null,
            'created_by' => $this->created_by,
            'title' => $this->title,
            'date' => $this->date,//Jalalian::fromDateTime($this->date)->toString(),
            'deadline' => $this->deadline,
            'explain' => $this->explain,
            'address' => (isset($this->address->address->province) ? $this->address->address->province : '').' '.(isset($this->address->address->city) ? $this->address->address->city : '').' '.(isset($this->address->address->region) ? $this->address->address->region : '').' '.(isset($this->address->address->address) ? $this->address->address->address : ''),
            'service_id' => $this->service_id,
        ];
    }
}
