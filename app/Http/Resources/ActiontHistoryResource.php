<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActiontHistoryResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        $user = $this->assignment->task->user;
        $address = $this->assignment->task->address->address;
        $files = $this->files;
        //return $address;
        $output = [
            'id' => $this->id,
            'user' => (isset($user->name) ? $user->name : '') . ' ' . (isset($user->family) ? $user->family : ''),
            'address' => (isset($address->province) ? $address->province : '') . ', ' . (isset($address->city) ? $address->city : '') . ', '.
            (isset($address->region) ? $address->region : '') . ', '. (isset($address->address) ? $address->address : ''),
            'image1' => $files[0]->file_path,
            'image2' => $files[1]->file_path,
            'value' => $this->description['description'],
            'created_at' => $this->created_at,
        ];

        return $output;
    }

}
