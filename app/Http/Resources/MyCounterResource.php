<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyCounterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => (isset($this->user) ? $this->user->name . ' '.$this->user->family : ''),
            'request_status' => isset($this->myBani->is_verified) ? ($this->myBani->is_verified == null ? 'pending' : ($this->myBani->is_verified == 0 ? 'rejected' : 'accepted')): 'pending',
            'request_id' => (isset($this->myBani->id) ? $this->myBani->id : ''),
            'province' => $this->province,
            'city' => $this->city,
            'address' => $this->address,
            'region' => $this->region,
            'counter' => $this->counter,
            'geo_place' => $this->geo_place,
            'counter_type' => $this->counter_type,
            'last_view' => $this->last_view,
            'status' => ( $this->status == null ? 'pending' : ( $this->status == 0 ? 'notValid' : 'valid' ) ),
        ];
    }
}
