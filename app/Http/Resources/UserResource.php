<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'family' => $this->family,
            'bani' => [
                'shaba' => isset($this->bani) ? $this->bani->shaba : '',
                'account_number' => isset($this->bani) ? $this->bani->account_number : '',
                'card_id' => isset($this->bani) ? $this->bani->card_id : '',
            ],
            'national_id' => $this->national_id,
            'email' => $this->email,
            'code' => $this->api_id,
            'status' => $this->status,
            'bellman_status' => (isset($this->bani) ? ($this->bani->count() > 0 ? ($this->bani->validate == 1 ? 2 : 1) : 0) : 0),
            'first_login' => 0,
            'roles' => array_map(
                function ($role) {
                    return $role['name'];
                },
                $this->roles->toArray()
            ),
            'permissions' => array_map(
                function ($permission) {
                    return $permission['name'];
                },
                $this->getAllPermissions()->toArray()
            ),
            'avatar' => 'https://i.pravatar.cc',
        ];
    }
}
