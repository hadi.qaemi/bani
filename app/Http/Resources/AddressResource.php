<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => (isset($this->user->name) ? $this->user->name : '') . ' '.(isset($this->user->family) ? $this->user->family : ''),
            'province' => $this->province,
            'city' => $this->city,
            'address' => $this->address,
            'region' => $this->region,
            'geo_place' => $this->geo_place,
            'last_view' => $this->last_view,
            'status' => ( $this->status == null ? 'pending' : ( $this->status == 0 ? 'notValid' : 'valid' ) ),
        ];
    }
}
