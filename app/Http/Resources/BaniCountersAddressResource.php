<?php

namespace App\Http\Resources;

use App\Models\Bani_Address;
use Illuminate\Http\Resources\Json\JsonResource;

class BaniCountersAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bani' => isset($this->bellman->user->name) ? $this->bellman->user->name . ' ' . $this->bellman->user->family : '',
        ];
    }
}
