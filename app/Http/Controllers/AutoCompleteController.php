<?php
/**
 * File PermissionController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;

use App\Laravue\Models\User;
use App\Models\Address;
use App\Models\Bani;
use App\Models\Bani_Address;
use App\Models\Region;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

/**
 * Class AutoCompleteController
 *
 * @package App\Http\Controllers
 */
class AutoCompleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function region(Request $request, Region $address)
    {
        $address = $address->autoCompleteRegion($request);
        return response()->json([
            'total_count' => count($address),
            'incomplete_results' => true,
            'items' => $address
        ],Response::HTTP_OK);
    }

    public function address(Request $request, Address $address)
    {
        $address = $address->autoCompleteAddress($request);
        return response()->json([
            'total_count' => count($address),
            'incomplete_results' => true,
            'items' => $address
        ],Response::HTTP_OK);
    }

    public function bani(Request $request, Bani $bani)
    {
        $bani = $bani->autoCompleteBani($request);
        return response()->json([
            'total_count' => count($bani),
            'incomplete_results' => true,
            'items' => $bani
        ],Response::HTTP_OK);
    }

}
