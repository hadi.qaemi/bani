<?php
/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;
use App\Laravue\JsonResponse;
use App\Laravue\Models\User;
use App\Models\Bani;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserResource;
use Kavenegar\KavenegarApi;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            $user = Auth::user();
            if($user->is_verified == 0)
                return response()->json(new JsonResponse([], 'account_not_verified'), Response::HTTP_FORBIDDEN);
            else if($user->status == 1)
                return response()->json(new UserResource(Auth::user()), Response::HTTP_OK)->header('Authorization', $token);
            else
                return response()->json(new JsonResponse([], 'account_not_activated'), Response::HTTP_FORBIDDEN);
        }
        $credential = [
            'email' => $request->input('email'),
            'sms_pass' => $request->input('password'),
        ];
        if ($token = $this->guard()->attempt($credentials)) {
            $user = Auth::user();
            if($user->is_verified == 0)
                return response()->json(new JsonResponse([], 'account_not_verified'), Response::HTTP_FORBIDDEN);
            else if($user->status == 1)
                return response()->json(new UserResource(Auth::user()), Response::HTTP_OK)->header('Authorization', $token);
            else
                return response()->json(new JsonResponse([], 'account_not_activated'), Response::HTTP_FORBIDDEN);
        }
        return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
    }

    public function loginMobile(Request $request)
    {
        $request->merge(
            [
                'email' => $request->get('phone_number')
            ]
        );
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            $user = Auth::user();
            if($user->is_verified == 0)
                return response()->json(new JsonResponse([], 'account_not_verified'), Response::HTTP_NOT_ACCEPTABLE);
            else if($user->status == 1){
                return response()->json([
                    'token' => $token,
                    'user' => $user->name. ' '.$user->family,
                    'first_login' => 0,
                    'tasks' => 1,
                    'bellman_status' => (isset($user->bani) ? $user->bani->count() > 0 ? ($user->bani->validate == 1 ? 2 : 1) : 0 : 0),
                ], Response::HTTP_OK);
            }
            else
                return response()->json(new JsonResponse([], 'account_not_activated'), Response::HTTP_NOT_ACCEPTABLE);
        }
        return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_BAD_REQUEST);
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function activeUser(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => ['required', 'exists:users,id']
            ]
        );
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $resultApi = $this->sendCurl('/profiles?status=VERIFIED', 'POST', '');
            $resultApi = \GuzzleHttp\json_decode($resultApi);
            if(isset($resultApi->status)){
                if(trim($resultApi->status) == 'VERIFIED'){
                    $user = User::find($request->input('id'));
                    $user->status = 1;
                    $user->api_id = $resultApi->profileId;
                    $user->save();
                    return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
                }
            }
            return response()->json((new JsonResponse())->success([]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function editBani(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => ['required', 'exists:users,id']
            ]
        );
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $user = User::find($request->input('id'));
            $user->name = $request->get('name');
            $user->family = $request->get('family');
            $user->national_id = $request->get('nationalcode');
            $user->status = 1;
            $user->save();

            $user->bani->shaba = $request->get('shabaNumber');
            $user->bani->card_id = $request->get('accountId');
            $user->bani->account_number = $request->get('accountNumber');
            $user->bani->save();
            return response()->json((new JsonResponse(['success' => true]))->success([]), Response::HTTP_OK);
        }
        return response()->json((new JsonResponse())->success([]), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function deactiveUser(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => ['required', 'exists:users,id']
            ]
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $user = User::find($request->input('id'));
            $user->status = 0;
            $user->save();
            return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
        }
    }

    /**
     * Display a show wallet.
     *
     * @return UserResource
     */

    public function registerMobile(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
//                    'image' => ['required'],
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            ),
            $this->customErrorMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $name = '';
//            if($request->get('image'))
//            {
//                $image = $request->get('image');
//                $name = 'images/profile/'.time().'-'.mt_rand(100000000000, 1000000000000).'-'.'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
//                \Image::make($request->get('image'))->save(public_path('/').$name);
//            }
//            if(isset($_FILES['image'])){
//                $name = $this->myUploadfile($_FILES['image'], 'profile');
//            }
            $params = $request->all();
            $message = random_int(20000,99999);
            $user = User::create([
                'name' => $params['name'],
                'family' => $params['family'],
                'national_id' => $params['nationalcode'],
                'national_id_pic' => $name,
                'verification_code' => $message,
                'email' => $params['mobile'],
                'is_verified' => 1,
                'mobile' => $params['mobile'],
                'introducer' => $params['introducer'],
                'password' => Hash::make($params['password']),
            ]);
            $user->syncRoles('user');

            Bani::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'uid' => $user->id,
                    'account_number' => '',
                    'card_id' => '',
                    'shaba' => '',
                    'validate' => 1,
                ]
            );

            $receptor[] = $params['mobile'];
            $this->sendSms($receptor, $message);
            return response()->json(['success' => true, 'user' => new UserResource($user)], 200);
        }
    }

    public function verificationMobile(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'token' => ['required', 'max:6'],
                'phone_number' => 'required|exists:users,mobile'
            ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $User = User::where('mobile', $request->get('phone_number'))->first();
            if($User->verification_code == $request->get('token')){
                $User->is_verified = 1;
                $User->save();
                return response()->json(['message ' => 'VERIFY_SUCCESSFULLY'], Response::HTTP_OK);
            }
            return response()->json(['message ' => 'FAIL'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function takeVerificationMobile(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'phone_number' => 'required|exists:users,mobile'
            ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $message = random_int(20000,99999);
            $User = User::where('mobile', $request->get('phone_number'))->first();
            $User->verification_code = $message;
            $this->sendSms($request->get('phone_number'), $message);

            if($User->save()){
                return response()->json(['message ' => 'VERIFICATION_SEND_SUCCESSFULLY'], Response::HTTP_OK);
            }
            return response()->json(['message ' => 'FAIL'], Response::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Display a show wallet.
     *
     * @return UserResource
     */

    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
//                    'image' => ['required'],
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            ),
            $this->customErrorMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $name = '';
//            if($request->get('image'))
//            {
//                $image = $request->get('image');
//                $name = 'images/profile/'.time().'-'.mt_rand(100000000000, 1000000000000).'-'.'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
//                \Image::make($request->get('image'))->save(public_path('/').$name);
//            }
//            if(isset($_FILES['image'])){
//                $name = $this->myUploadfile($_FILES['image'], 'profile');
//            }
            $params = $request->all();
            $message = random_int(20000,99999);
            $user = User::create([
                'name' => $params['name'],
                'family' => $params['family'],
                'national_id' => $params['nationalcode'],
                'national_id_pic' => $name,
                'verification_code' => $message,
                'email' => $params['mobile'],
                'mobile' => $params['mobile'],
                'introducer' => $params['introducer'],
                'is_verified' => 1,
                'password' => Hash::make($params['password']),
            ]);
            $user->syncRoles('user');

            Bani::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'uid' => $user->id,
                    'account_number' => '',
                    'card_id' => '',
                    'shaba' => '',
                    'validate' => 1,
                ]
            );

            $receptor[] = $params['mobile'];
            $this->sendSms($receptor, $message);
            return response()->json(['success' => true, 'user' => new UserResource($user)], 200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newVerificationCode(Request $request)
    {
        $customMessages = [
            'required' => 'validators,:attribute,required'
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'mobile' => 'required|exists:users,mobile'
            ]
            ,
            $customMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $User = User::where('mobile', $request->get('mobile'))->first();
            $User->verification_code = random_int(20000,99999);
            $User->save();

            $sender = env('KAVENEGAR_SENDER');
            $receptor[] = $request->get('mobile');
            $KavenegarApi = new KavenegarApi(env('KAVENEGAR_KEY_ID'));
            $KavenegarApi->Send($sender,$receptor,$User->verification_code);
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function smsPwd(Request $request)
    {
        $customMessages = [
            'required' => 'validators,:attribute,required'
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|exists:users,email'
            ]
            ,
            $customMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $User = User::where('email', $request->get('email'))->first();
            $random_int = random_int(20000,99999);
            $User->password = Hash::make($random_int);//;
            $User->save();

            $sender = env('KAVENEGAR_SENDER');
            $receptor[] = $User->mobile;
            $KavenegarApi = new KavenegarApi(env('KAVENEGAR_KEY_ID'));
            $KavenegarApi->Send($sender,$receptor,$random_int);
            return ['success' => true];
        }
        return ['success' => false];
    }

    /**
     * Display a show wallet.
     *
     * @return UserResource
     */


    public function verification(Request $request)
    {
        $customMessages = [
            'required' => 'validators,:attribute,required'
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'verification_code' => ['required', 'max:6'],
                'mobile' => 'required|exists:users,mobile'
            ]
            ,
            $customMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $User = User::where('mobile', $request->get('mobile'))->first();
            if($User->verification_code == $request->get('verification_code')){
                $User->is_verified = 1;
                $User->save();
                return ['success' => true];
            }
            return ['success' => false];
        }
        $this->guard()->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'family' => 'required',
            'nationalcode' => 'required|numeric|unique:users,national_id',
            'mobile' => 'required|numeric|unique:users,mobile',
        ];
    }

    public function user()
    {
        return new UserResource(Auth::user());
    }

    /**
     * @return mixed
     */
    private function guard()
    {
        return Auth::guard();
    }
}
