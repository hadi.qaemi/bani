<?php
/**
 * File PermissionController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;

use App\Http\Resources\TaskCounterResource;
use App\Http\Resources\TaskResource;
use App\Models\Address;
use App\Models\Address_Counter;
use App\Models\Assignment;
use App\Models\Assignment_History;
use App\Models\Bani_Address;
use App\Models\History_Description;
use App\Models\History_File;
use App\Models\Qrcode;
use App\Models\Task;
use App\Models\Task_Address;
use App\Models\Task_Address_Counter;
use App\Models\Task_Address_Counter_File;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use DataTables;
use Morilog\Jalali\Jalalian;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{
    const ITEM_PER_PAGE = 15;
    const COUNTER_STATUS_READ = 'read';
    const COUNTER_STATUS_UNREAD = 'unread';
    const DONE_TASK = 'done';
    const WAITING_FOR_BELLMAN = 'waiting_for_bellman';
    const CANCEL_BY_BELLMAN = 'cancel_by_bellman';
    const WAITEING_FOR_ACTION = 'waiting_for_action';
    const WAITEING_FOR_EDIT = 'waiting_for_edit';
    const SELF_ASSIGNMENT_TILTE = 'SELF_ASSIGNMENT_TILTE';
    const SELF_ASSIGNMENT_EXPLAIN = 'SELF_ASSIGNMENT_EXPLAIN';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $taskQuery = Task::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $tasks = TaskResource::collection($taskQuery->paginate($limit));
        return response()->json(
            [
                'status' => 'success',
                'result' => [
                    'total' => count($tasks),
                    'rows' => $tasks
                ],
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendAction(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
//                'qrcode' => 'required|exists:qrcodes,code',
//                'qrcode' => 'required',
                'assignment_id' => 'exists:assignments,id',
                'code' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        if($request->exists('assignment_id')){
            $assignment_id = $request->get('assignment_id');
        }else{

            $Task = Task::create([
                'user_id' => auth()->id(),
                'date' => date('Y-m-d H:i:s'),
                'title' => self::SELF_ASSIGNMENT_TILTE,
                'explain' => self::SELF_ASSIGNMENT_EXPLAIN,
                'deadline' => 1,
            ]);

            $Qrcode = Address_Counter::where('qr_code', '=', $request->get('qrcode'))
                ->first()
            ;
//            $Qrcode->status = 'read';
//            $Qrcode->save();

            if ($Qrcode->count() == 0) {
                return response()->json(['errors' => 'Qrcode not found'], 404);
            }

            $Task_Address = Task_Address::create([
                'address_id' => $Qrcode->address_id,
                'task_id' => $Task->id,
            ]);

            $TaskAddressCounters = Task_Address_Counter::create([
                'task_has_address_id' => $Task_Address->id,
                'addresses_has_counter_id' => $Qrcode->id,
            ]);
            $assignment_id = $TaskAddressCounters->id;
        }
        $image1 = null;
        if(isset($_FILES['image1'])){
            $image1 = $this->myUploadfile($_FILES['image1'], 'scan');
        }

        $image2 = null;
        if(isset($_FILES['image2'])){
            $image2 = $this->myUploadfile($_FILES['image2'], 'scan');
        }
        Task_Address_Counter_File::create([
            'task_has_address_has_counter_id' => $assignment_id,
            'user_id' => auth()->id(),
            'file_path' => $image1,
        ]);

        Task_Address_Counter_File::create([
            'task_has_address_has_counter_id' => $assignment_id,
            'user_id' => auth()->id(),
            'file_path' => $image2,
        ]);

        Task_Address_Counter::where('id', $assignment_id)->update([
            'bani_id' => auth()->id,
            'status' => self::DONE_TASK,
            'value' => $request->get('code')
        ]);
        return response()->json(
            [
                'success' => true,
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendActionByMobile(Request $request) {
        $validator = Validator::make($request->all(),
            [
//                'qrcode' => 'required|exists:qrcodes,code',
//                'qrcode' => 'required',
                'assignment_id' => 'exists:task_has_address_has_counters,id',
                'code' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $code = trim(str_ireplace('https://energynote.ir/b/', '', $request->get('qrcode')));
        $Qrcode = Address_Counter::where('qr_code', '=', $code)
            ->first()
        ;
        if($request->exists('assignment_id')){
            $assignment_id = $request->get('assignment_id');
            $TaskAddressCounters2 = Task_Address_Counter::find($assignment_id);
            if($TaskAddressCounters2->status !== self::DONE_TASK){
                $TaskAddressCounters2->counter->status = self::COUNTER_STATUS_READ;
                $TaskAddressCounters2->counter->save();
            }
        }else{

            $Task = Task::create([
                'user_id' => auth()->id(),
                'date' => date('Y-m-d H:i:s'),
                'title' => self::SELF_ASSIGNMENT_TILTE,
                'explain' => self::SELF_ASSIGNMENT_EXPLAIN,
                'deadline' => 1,
            ]);

            if ($Qrcode == null) {
                return response()->json(
                    [
                        'success' => false,
                        'data' => [],
                        'errors' => 'Qrcode_not_valid'
                    ]
                    , Response::HTTP_NOT_FOUND);
            }

            $Task_Address = Task_Address::create([
                'address_id' => $Qrcode->address_id,
                'task_id' => $Task->id,
            ]);

            $TaskAddressCounters = Task_Address_Counter::create([
                'task_has_address_id' => $Task_Address->id,
                'addresses_has_counter_id' => $Qrcode->id,
            ]);
            $assignment_id = $TaskAddressCounters->id;
        }
        $image1 = null;
        if(isset($_FILES['image1'])){
            $image1 = $this->myUploadfile($_FILES['image1'], 'scan');
        }

        $image2 = null;
        if(isset($_FILES['image2'])){
            $image2 = $this->myUploadfile($_FILES['image2'], 'scan');
        }

        Task_Address_Counter_File::create([
            'task_has_address_has_counter_id' => $assignment_id,
            'user_id' => auth()->id(),
            'file_path' => $image1,
        ]);

        Task_Address_Counter_File::create([
            'task_has_address_has_counter_id' => $assignment_id,
            'user_id' => auth()->id(),
            'file_path' => $image2,
        ]);
        Task_Address_Counter::where('id', $assignment_id)->update([
            'bani_id' => auth()->id(),
            'status' => self::DONE_TASK,
            'value' => $request->get('code')
        ]);

        return response()->json(
            [
                'success' => true,
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllActions(Request $request) {
        $assignments = Assignment::all()->where('last_status', 'done');
        $result = [];
        foreach ($assignments as $assignment) {
            $lastHistory = \App\Models\Assignment::find($assignment->id)->lastHistory();
            //return ($lastHistory);
            if ($lastHistory !== null)
                $result[] = new \App\Http\Resources\ActiontHistoryResource($lastHistory);
        }
        return response()->json(
            [
                'status' => 'success',
                'result' => [
                    $result
                ],
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    public function updateActionValue(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:assignment_has_history,id',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        History_Description::where('assignment_has_history_id', $request->get('id'))
            ->update([
                'description' => $request->get('value')
        ]);


        return response()->json([
            'status' => 'success',
            'messages' => null
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myTasks(Request $request) {
        $searchParams = $request->all();

        $taskQuery = Task_Address_Counter::whereNull('status')
            ->whereExists(function ($query) {
                $query->select("*")
                    ->from('task_has_addresses')
                    ->whereRaw('task_has_addresses.id = task_has_address_has_counters.task_has_address_id')
                    ->whereExists(function ($query) {
                        $query->select("*")
                            ->from('selected_bellman_service')
                            ->whereRaw('task_has_addresses.address_id = selected_bellman_service.user_has_addresses_id')
                            ->where('bellman_id', '=', auth()->user()->bani->id);
                        ;
                    })
            ;
        });
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $tasks = TaskCounterResource::collection($taskQuery->paginate($limit));

        return response()->json(
            [
                'status' => 'success',
                'result' => [
                    'total' => count($tasks),
                    'rows' => $tasks
                ],
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myTasksCount(Request $request) {
        $searchParams = $request->all();

        $taskQuery = Task_Address_Counter::whereNull('status')
            ->whereExists(function ($query) {
            $query->select("*")
                ->from('task_has_addresses')
                ->whereRaw('task_has_addresses.id = task_has_address_has_counters.task_has_address_id')
                ->whereExists(function ($query) {
                    $query->select("*")
                        ->from('selected_bellman_service')
                        ->whereRaw('task_has_addresses.address_id = selected_bellman_service.user_has_addresses_id')
                        ->where('bellman_id', '=', auth()->user()->bani->id);
                    ;
                })
            ;
        });

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $tasks = TaskCounterResource::collection($taskQuery->paginate($limit));

        return response()->json(
            [
                'status' => 'success',
                'result' => [
                    'count' => count($tasks),
                ],
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function removeAssignment(Request $request)
    {
        $Assignment = Assignment::find($request->get('assignment_id'));
        $Assignment->last_status = self::CANCEL_BY_BELLMAN;
        $Assignment->save();
        Assignment_History::create([
            'assignment_id' => $request->get('assignment_id'),
            'user_id' => auth()->id(),
            'status' => self::CANCEL_BY_BELLMAN,
        ]);
        return response()->json(
            [
                'success' => true,
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function acceptAssignment(Request $request)
    {
//        $Assignment = Assignment::find($request->get('assignment_id'));
//        $Assignment->last_status = self::WAITEING_FOR_ACTION;
//        $Assignment->save();
//        Assignment_History::create([
//            'assignment_id' => $request->get('assignment_id'),
//            'user_id' => auth()->id(),
//            'status' => self::WAITEING_FOR_ACTION,
//        ]);
        return response()->json(
            [
                'success' => true,
                'messages' => null
            ]
            , Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'explain' => 'required|max:255',
            'date' => 'required|max:255',
            'region' => 'required_without_all:address',
            'address' => 'required_without_all:region',
//            'bani' => 'required_without_all:region,address',
            'deadline' => 'required|max:255',
        ], $this->customErrorMessages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $region = $request->get('region');

        $bani = $request->get('bani');


        $Task = Task::create([
            'user_id' => auth()->id(),
            'date' => \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y/m/d H:i', $request->get('date'))->format('Y-m-d H:i:s'),
            'title' => $request->get('title'),
            'explain' => $request->get('explain'),
            'deadline' => $request->get('deadline'),
        ]);
        if($request->get('address')){

            $address = $request->get('address');
            $address = Address::find($address['id']);

            $Task_Address = Task_Address::create([
                'address_id' => $address->id,
                'task_id' => $Task->id,
            ]);

            foreach ($address->counters as $counter){
                $counter->status = self::COUNTER_STATUS_UNREAD;
                $counter->save();
                Task_Address_Counter::create([
                    'task_has_address_id' => $Task_Address->id,
                    'addresses_has_counter_id' => $counter->id,
                ]);
            }

        }else{
            $address = Address::with('counters')
                ->where('region', $region)->get();

            foreach($address as $item){
                $Task_Address = Task_Address::create([
                    'address_id' => $item->id,
                    'task_id' => $Task->id,
                ]);

                foreach ($item->counters as $counter){
                    $counter->status = self::COUNTER_STATUS_UNREAD;
                    $counter->save();
                    Task_Address_Counter::create([
                        'task_has_address_id' => $Task_Address->id,
                        'addresses_has_counter_id' => $counter->id,
                    ]);
                }
            }
        }

        return response()->json(['success' => true], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
