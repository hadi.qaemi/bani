<?php
/**
 * File PermissionController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;

use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Models\Token;
use App\Models\Token_Wallet;
use App\Models\Uses;
use App\Models\Wallet;
use App\Laravue\Models\User;
use Illuminate\Http\Request;

use App\Laravue\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Response;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers
 */
class BuyController extends Controller
{
    /**
     * Display a show wallet.
     *
     * @return \Illuminate\Http\Response
     */

    public function all($request = '')
    {
        $tokens = Token_Wallet::select(['*','wallets.title as wtitle','token_wallet.id as wtid','tokens.title as ttitle'])
            ->leftJoin('tokens', 'token_wallet.token_id', '=', 'tokens.id')
            ->leftJoin('wallets', 'token_wallet.wallet_id', '=', 'wallets.id')
            ->get();
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => count($tokens),
                'rows' => $tokens
            ],
            'messages' => null
        ]);
    }


    /**
     * Display a show wallet.
     *
     * @return \Illuminate\Http\Response
     */

    public function confirmToken(Request $request)
    {
        $Token_Wallet = Token_Wallet::find($request->input('id'));
        $res =$Token_Wallet->update([
            'status' => 1
        ]);
        return response()->json([
            'status' => 'success',
            'messages' => null
        ]);
    }

    /**
     * Display a show wallet.
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteToken(Request $request)
    {
        $Token_Wallet = Token_Wallet::find($request->input('id'));
        $res =$Token_Wallet->update([
            'status' => 2
        ]);
        return response()->json([
            'status' => 'success',
            'messages' => null
        ]);
    }

    /**
     * Display a show wallet.
     *
     * @return \Illuminate\Http\Response
     */

    public function mine($request = '')
    {
        $user = User::find(auth()->id());
        $uses = Uses::where('number', '=', $user->subscription_number)
            ->get();
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => count($uses),
                'rows' => $uses
            ],
            'messages' => null
        ]);

        $resultApi = $this->sendCurl('/coins/profiles/'.auth()->user()->getApiId().'/wallets', 'GET');
        $resultApi = json_decode($resultApi);
        $resultApi = $this->sendCurl('/coins/transfers/'.$resultApi->publicAddress, 'GET');
        if($this->isJson($resultApi)){
            $resultApi = \GuzzleHttp\json_decode($resultApi);
            if(count($resultApi) > 0){
                return response()->json([
                    'status' => 'success',
                    'result' => [
                        'total' => count($resultApi),
                        'rows' => $resultApi
                    ],
                    'messages' => null
                ], Response::HTTP_OK);
            }
        }
        return response()->json([], Response::HTTP_INTERNAL_SERVER_ERROR);
        dd($request->all());
        $tokens = Token_Wallet::select(['*','wallets.title as wtitle','tokens.title as ttitle'])
            ->leftJoin('tokens', 'token_wallet.token_id', '=', 'tokens.id')
            ->leftJoin('wallets', 'token_wallet.wallet_id', '=', 'wallets.id')
            ->where('token_wallet.uid', '=', auth()->id())
            ->get();
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => count($tokens),
                'rows' => $tokens
            ],
            'messages' => null
        ]);
    }

    /**
     * Display a fetch wallet tokens.
     *
     * @return \Illuminate\Http\Response
     */

    public function fetchTokens($id)
    {
        $Token_Wallet = Token_Wallet::leftJoin('tokens', 'token_wallet.token_id', '=', 'tokens.id')
            ->where('wallet_id', '=', decrypt($id))->get();
        return response()->json(
            [
                'tokens' => $Token_Wallet,
            ]
            , 200);
    }

    /**
     * Display a add token.
     *
     * @return \Illuminate\Http\Response
     */

    public function addToken(Request $request)
    {
        $request->merge(['token_id' => decrypt($request->input('token_id'))]);
        $request->merge(['wallet_id' => decrypt($request->input('wallet_id'))]);

        $validator = Validator::make($request->all(), [
            'wallet_id' => 'required|exists:wallets,id',
            'token_id' => 'required|exists:tokens,id',
            'amount' => 'required|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $request->merge(['uid' => auth()->id()]);
        $request->merge(['status' => 0]);
        return response()->json(Token_Wallet::addToken($request->all()), 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, Wallet $wallet)
    {
        $wallets = $wallet->getWallets($request);
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $wallets->total(),
                'rows' => $wallets->items()
            ],
            'messages' => null
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, Wallet $wallet)
    {
        $wallets = $wallet->getWallets($request);
        return response()->json([
            'status' => 'success',
            'result' => [
                'total' => $wallets->total(),
                'rows' => $wallets->items()
            ],
            'messages' => null
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:50',
            'explain' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $request->merge(['uid' => auth()->id()]);
        $request->merge(['address' => 'WT-'.Str::random(8)]);
        return response()->json(Wallet::addWallet($request->all()), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
