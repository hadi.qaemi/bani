<?php
/**
 * File UserController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Http\Controllers;

use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use App\Laravue\Models\Permission;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use App\Models\Bani;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Validator;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    const ITEM_PER_PAGE = 15;

    public function registerBani(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
                [
                    'accountNumber' => ['required', 'numeric'],
                    'accountId' => ['required', 'numeric'],
                    'shabaNumber' => ['required', 'numeric'],
                ],
            $this->customErrorMessages
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => false,
                    'errors' => $validator->errors(),
                ]
                , Response::HTTP_BAD_REQUEST);
        } else {
            $params = $request->all();
            Bani::updateOrCreate(
                ['user_id' => auth()->id()],
                ['account_number' => $params['accountNumber'],
                    'card_id' => $params['accountId'],
                    'shaba' => $params['shabaNumber']
                ]
            );
            return response()->json(
                [
                    'success' => true,
                    'messages' => null
                ]
                , Response::HTTP_OK);
        }
    }

    /**
     * Display a listing of the user resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|ResourceCollection
     */
    public function sendForgetPasswordMobile(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone_number' => ['required', 'exists:users,mobile']
            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $User = User::where('mobile', $request->get('phone_number'))->first();
        $User->verification_code = random_int(20000,99999);
        $User->save();
        $this->sendSms($request->input('phone_number'), $User->verification_code);
        return response()->json(['message' => 'SEND_VERCODE_SUCCESSFUL'], 200);
    }
    public function sendForgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'mobile' => ['required', 'exists:users,mobile']
            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $User = User::where('mobile', $request->get('mobile'))->first();
        $User->verification_code = random_int(20000,99999);
        $User->save();
        $this->sendSms($request->input('mobile'), $User->verification_code);
        return response()->json(['success' => true], 200);
    }

    public function verifyForgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'mobile' => ['required', 'exists:users,mobile'],
                'verification_code' => ['required', 'numeric'],
                'password' => ['required', 'min:6'],
                'confirmPassword' => 'same:password',
            ],
            $this->customErrorMessages
        );
        if ($validator->fails())
        {
            return response()->json(['errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $User = User::where('mobile', $request->get('mobile'))->first();
        if($request->get('verification_code') == $User->verification_code)
        {
            $User->password = Hash::make($request->get('password'));
            if($User->save())
                return response()->json(['success' => true], 200);
        }else{
            return response()->json(['errors' => 'validators,verification_code,same'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function verifyForgetPasswordMobile(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone_number' => ['required', 'exists:users,mobile'],
                'reset_token' => ['required', 'numeric', 'exists:users,verification_code'],
                'password' => ['required', 'min:6'],
                'confirm_password' => 'same:password',
            ]
        );
        if ($validator->fails())
        {
            return response()->json(['errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $User = User::where('mobile', $request->get('phone_number'))->first();
        if($request->get('reset_token') == $User->verification_code)
        {
            $User->password = Hash::make($request->get('password'));
            if($User->save())
                return response()->json(['message' => 'VERIFIED_SUCCESSFUL'], 200);
        }else{
            return response()->json(['errors' => 'validators,verification_code,same'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function registerUserBani(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                [
//                    'image' => ['required'],
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                    'name' => 'required',
                    'family' => 'required',
                    'nationalcode' => 'required|numeric|unique:users,national_id',
                    'mobile' => 'required|numeric|unique:users,mobile',
                    'accountNumber' => ['required', 'numeric'],
                    'accountId' => ['required', 'numeric'],
                    'shabaNumber' => ['required', 'numeric'],
                ]
            ),
            $this->customErrorMessages
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], Response::HTTP_BAD_REQUEST);
        } else {
            $name = '';
            if ($request->get('image')) {
                $image = $request->get('image');
                $name = 'images/profile/' . time() . '-' . mt_rand(100000000000, 1000000000000) . '-' . '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('/') . $name);
            }
            if (isset($_FILES['image'])) {
                $name = $this->myUploadfile($_FILES['image'], 'profile');
            }
            $params = $request->all();
            $message = random_int(20000, 99999);
            $user = User::create([
                'name' => $params['name'],
                'family' => $params['family'],
                'national_id' => $params['nationalcode'],
                'national_id_pic' => $name,
                'verification_code' => $message,
                'is_verified' => 1,
                'email' => $params['mobile'],
                'mobile' => $params['mobile'],
                'password' => Hash::make($params['password']),
            ]);
            $user->syncRoles('user');

            $receptor[] = $params['mobile'];

            //$this->sendSms($receptor, $message);

            Bani::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'uid' => auth()->id(),
                    'account_number' => $params['accountNumber'],
                    'card_id' => $params['accountId'],
                    'shaba' => $params['shabaNumber'],
                    'validate' => 1,
                ]
            );

            return response()->json(['success' => true, 'user' => new UserResource($user)], 200);
        }
    }

    /**
     * Display a listing of the user resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|ResourceCollection
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $userQuery = User::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $role = Arr::get($searchParams, 'role', '');
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($role)) {
            $userQuery->whereHas('roles', function($q) use ($role) { $q->where('name', $role); });
        }

        if (!empty($keyword)) {
            $userQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $userQuery->where('email', 'LIKE', '%' . $keyword . '%');
        }

        return UserResource::collection($userQuery->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $params = $request->all();
            $user = User::create([
                'name' => $params['name'],
                'email' => $params['email'],
                'password' => Hash::make($params['password']),
            ]);
            $role = Role::findByName($params['role']);
            $user->syncRoles($role);

            return new UserResource($user);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User    $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if ($user === null) {
            return response()->json(['errors' => 'User not found'], 404);
        }
        if ($user->isAdmin()) {
            return response()->json(['errors' => 'Admin can not be modified'], 403);
        }

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $email = $request->get('email');
            $found = User::where('email', $email)->first();
            if ($found && $found->id !== $user->id) {
                return response()->json(['errors' => 'Email has been taken'], 403);
            }

            $user->name = $request->get('name');
            $user->email = $email;
            $user->save();
            return new UserResource($user);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User    $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function updatePermissions(Request $request, User $user)
    {
        if ($user === null) {
            return response()->json(['errors' => 'User not found'], 404);
        }

        if ($user->isAdmin()) {
            return response()->json(['errors' => 'Admin can not be modified'], 403);
        }

        $permissionIds = $request->get('permissions', []);
        $rolePermissionIds = array_map(
            function($permission) {
                return $permission['id'];
            },

            $user->getPermissionsViaRoles()->toArray()
        );

        $newPermissionIds = array_diff($permissionIds, $rolePermissionIds);
        $permissions = Permission::allowed()->whereIn('id', $newPermissionIds)->get();
        $user->syncPermissions($permissions);
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->isAdmin()) {
            response()->json(['errors' => 'Ehhh! Can not delete admin user'], 403);
        }

        try {
            $user->delete();
        } catch (\Exception $ex) {
            response()->json(['errors' => $ex->getMessage()], 403);
        }

        return response()->json(null, 204);
    }

    /**
     * Get permissions from role
     *
     * @param User $user
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function permissions(User $user)
    {
        try {
            return new JsonResponse([
                'user' => PermissionResource::collection($user->getDirectPermissions()),
                'role' => PermissionResource::collection($user->getPermissionsViaRoles()),
            ]);
        } catch (\Exception $ex) {
            response()->json(['errors' => $ex->getMessage()], 403);
        }
    }

    /**
     * @param bool $isNew
     * @return array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required',
            'email' => $isNew ? 'required|email|unique:users' : 'required|email',
            'roles' => [
                'required',
                'array'
            ],
        ];
    }
}
