<?php

/**
 * File PermissionController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Http\Controllers;

use App\Http\Resources\AddressCountersResource;
use App\Http\Resources\AssignmentHistoryResource;
use App\Http\Resources\BaniCountersAddressResource;
use App\Http\Resources\CounterResource;
use App\Http\Resources\LogResource;
use App\Http\Resources\TaskResource;
use App\Models\Address;
use App\Models\Address_Attribute;
use App\Models\Address_Counter;
use App\Models\Assignment_History;
use App\Models\Bani;
use App\Models\Bani_Address;
use App\Models\Bani_Self_Address;
use App\Models\Manual_Address;
use App\Models\Qrcode;
use App\Models\Task;
use App\Models\Task_Address;
use App\Models\Task_Address_Counter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Morilog\Jalali\Jalalian;
use Yajra\DataTables\DataTables;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers
 */
class AddressController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createQrcodes(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'numberQrcodes' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $rand = [];
        for ($i = 0; $i < $request->get('numberQrcodes'); $i++) {
            $rand[]['id'] = mt_rand(1000000, 10000000);
            Qrcode::create([
                'code' => $rand[count($rand) - 1]['id'],
            ]);
        }
        return response()->json([
            'success' => true,
            'codes' => $rand
        ], Response::HTTP_OK);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selected(Request $request, Address $address)
    {
        $tokens = $address->getBaniAddresess($request);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => $tokens->total(),
                'rows' => $tokens->items()
            ],
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectedAddress($id, Address $address)
    {
        $Bani_Address = Bani_Address::where('bellman_id', auth()->user()->bani->id)
            ->where('user_has_addresses_id', $id)
            ->first();
        return response()->json([
            'success' => true,
            'result' => [
                'total' => 1,
                'rows' => [
                    [
                        'id' => $Bani_Address->id,
                        'role' => $Bani_Address->associate_type,
                        'is_verified' => $Bani_Address->is_verified == null ? 1 : 1,
                        'has_qrcode' => $Bani_Address->address->has_qrcode == null ? 1 : 1
                    ]
                ]
            ],
            'messages' => null
        ], Response::HTTP_OK);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendVerification($id, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone_number' => ['required', 'exists:users,mobile'],

            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 400);
        }
        $bani_address = Bani_Address::where('bellman_id', auth()->id())
            ->where('user_has_addresses_id', $id)
        ;
        $verificationCode = random_int(20000, 99999);
        if ($bani_address->count()) {
            $bani_address = $bani_address->first();
            $bani_address->bellman_id = auth()->id();
            $bani_address->user_has_addresses_id = $id;
            $bani_address->verification_code = $verificationCode ;
            $bani_address->save();
        }else{
            Bani_Address::create([
                'bellman_id' => auth()->id(),
                'user_has_addresses_id' => $id,
                'verification_code' => $verificationCode
            ]);
        }
        $this->sendSms($request->input('phone_number'), $verificationCode);
        return response()->json([
            'success' => true,
            'messages' => null
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addOwner($address, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'code' => ['required', 'exists:qrcodes,code'],
                'verification' => ['required', 'exists:selected_bellman_service,verification_code']
            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 400);
        }
        $address_id = Qrcode::where('code', $request->post('code'))->first()->subscription_number;
        if ($address_id != $address) {
            return response()->json(['errors' => 'validators,code,same'], 400);
        }
        $baniAddress = Bani_Address::where('user_has_address_id', $address)
            ->where('bellman_has_service_id', auth()->id())->first();
        if ($baniAddress->verification_code == $request->post('code')) {
            $baniAddress->associate_type = 'Owner';
            $baniAddress->is_verified = 1;
            $baniAddress->updated_at = date("Y-m-d H:m:s", time());
            if ($baniAddress->save()) {
                return response()->json([
                    'success' => true,
                    'messages' => null
                ], Response::HTTP_OK);
            }
        } else {
            return response()->json(['errors' => 'validators,verification_code,same'], 400);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addRenter($address, Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'code' => ['required', 'exists:qrcodes,code'],
                'verification' => ['required', 'exists:selected_bellman_service,verification_code']
            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()], 400);
        }
        $address_id = Qrcode::where('code', $request->post('code'))->first()->subscription_number;
        if ($address_id != $address) {
            return response()->json(['errors' => 'validators,code,same'], 400);
        }
        $baniAddress = Bani_Address::where('user_has_address_id', $address)
            ->where('bellman_has_service_id', auth()->id())->first();
        if ($baniAddress->verification_code == $request->post('code')) {
            $baniAddress->associate_type = 'Renter';
            $baniAddress->is_verified = 1;
            $baniAddress->updated_at = date("Y-m-d H:m:s", time());
            if ($baniAddress->save()) {
                return response()->json([
                    'success' => true,
                    'messages' => null
                ], Response::HTTP_OK);
            }
        } else {
            return response()->json(['errors' => 'validators,verification_code,same'], 400);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addSupport($id, Request $request)
    {

        $bani_address = Bani_Address::updateOrCreate(
            ['user_has_addresses_id' => $id, 'bellman_id' => auth()->id()],
            ['associate_type' => 'Support']
        );

        if(isset($bani_address->address)){
            $bani_address->address->has_qrcode = $request->get('hasQrcode');
            $bani_address->address->save();
        }
        Bani_Self_Address::updateOrCreate(
            ['bellman_id' => auth()->user()->bani->id],
            ['geolocation' => $request->post('geolocation'), 'address' => $request->get('address')]
        );

        return response()->json(['success' => true,
            'messages' => null], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addQrCode($id, Request $request)
    {

        Bani_Address::updateOrCreate(
            ['user_has_addresses_id' => $id, 'bellman_id' => auth()->id()],
            ['associate_type' => 'QrCode']
        );

        Bani_Self_Address::updateOrCreate(
            ['bellman_id' => auth()->user()->bani->id],
            ['geolocation' => $request->post('geolocation'), 'address' => $request->get('address')]
        );

        return response()->json([
            'success' => true,
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAddressCounter(Request $request) {
        return response()->json([
            'success' => true,
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function qrcodeSubscribtionCode(Request $request) {
        $Manual_Address = Manual_Address::where('powerMeter', 'like', $request->get('subscribtion_code').'%');
        if($Manual_Address->count()){
            $qrcode = $Manual_Address->first();
            return response()->json([
                'success' => true,
                'qrcode' => $qrcode->qrcode
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'success' => true,
                'qr_code' => '',
                'messages' => null
            ], Response::HTTP_OK);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setAttributeCounter(Request $request, Address $address) {
        $validator = Validator::make($request->all(),
            [
                'address_id' => 'required|exists:user_has_addresses,id',
                'qr_code' => 'required',
                'counter_type' => 'required|in:counter_old,counter_new',
                'subscribtion_code' => 'required',
            ], $this->customErrorMessages
        );
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->messages(), 'success' => false], 400);
        }

        $Address_Counter = Address_Counter::AddCounter(
            $request->get('address_id'),
            $request->get('qr_code'),
            $request->get('counter_type'),
            $request->get('metadata'),
            $request->get('subscribtion_code')
        );
        if($Address_Counter){
            return response()->json([
                'success' => true,
                'messages' => null
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'success' => false,
                'messages' => null
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function setAttributeAddress(Request $request, Address $address) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:user_has_addresses,id',
                'user_id' => 'required|exists:users,id',
                'province' => 'required',
                'city' => 'required',
                'region' => 'required',
                'address' => 'required',
//                'geo_place' => 'required',
                'row' => 'required|numeric',
                'column' => 'required|numeric',
            ],
            $this->customErrorMessages
        );
        if ($validator->fails()) {
            return response()->json(['success' => true, 'errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $address = $address->find($request->get('id'));
        $address->province = $request->get('province');
        $address->city = $request->get('city');
        $address->region = $request->get('region');
        $address->address = $request->get('address');
        $address->geo_place = $request->get('geo_place');
        $address->row = $request->get('row');
        $address->column = $request->get('column');
        if($address->save()){
            return response()->json([
                'success' => true,
                'messages' => null
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'success' => false,
                'messages' => null
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        }
    }

    /**
     * @param Request $request
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function addressCounters($id, Address $address)
    {
        $validator = Validator::make(
            [
                'id' => $id,
            ],
            [
                'id' => 'required|exists:user_has_addresses,id',
            ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $counters = $address->find($id)->counters;
        $counters = CounterResource::collection($counters);
        return response()->json([
            'success' => true,
            'result' => [
                'rows' => $counters
            ],

            'messages' => null
        ], Response::HTTP_OK);
    }

    public function countersByPosition($id, Address $address)
    {
        $validator = Validator::make(
            [ 'id' => $id, ],
            [ 'id' => 'required|exists:user_has_addresses,id', ],
            $this->customErrorMessages);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        $address = $address->find($id);
        $counters = $address->counters;
        $data = [];
        foreach ($counters as $counter)
            $data[$counter->row_column['row']][$counter->row_column['column']] = $counter->subscribtion_code . ', ' . $counter->qr_code;
        for ($i = 1; $i <= $address->row; $i++)
            for ($j = 1; $j <= $address->column; $j++)
                if(!isset($data[$i][$j]))
                    $data[$i][$j] = null;
        return response()->json([
            'success' => true,
            'counters' => $data,
            'messages' => null
        ], Response::HTTP_OK);
    }

    public function log($id, Address $address)
    {
        $log = Task_Address_Counter::whereExists(function ($query) use ($id){
            $query->select("*")
                ->from('task_has_addresses')
                ->whereRaw('task_has_addresses.id = task_has_address_has_counters.task_has_address_id')
                ->where('address_id', '=', $id)
                ->where('status', '=', 'done')
            ;
        })->get();
        return response()->json([
            'success' => true,
            'result' => [
                'rows' => LogResource::collection($log)
            ],

            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectedAddressCounter($address) {
        return response()->json([
            'success' => true,
            'result' => [
                'total' => 1,
                'rows' => [
                    [
                        'id' => 19,
                        'role' => null,
                        'has_qrcode' => 0
                    ]
                ]
            ],
            'messages' => null
        ], Response::HTTP_OK);
    }

    public function sendQrcode(Request $request, Qrcode $qrcode) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:qrcodes,id',
                'qrcodeSubscriptionNumber' => 'required|exists:user_has_addresses,counter',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $code = $qrcode->find($request->get('id'));
        $code->subscription_number = $request->get('qrcodeSubscriptionNumber');
        $code->save();
        if ($code) {
            return response()->json([
                'success' => true,
            ], Response::HTTP_OK);
        }
        return response()->json([
            'success' => false,
        ], Response::HTTP_BAD_REQUEST);
    }

    public function listQrcodes(Request $request, Qrcode $qrcode) {
        $qrcodes = $qrcode->getQrcodes($request);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => $qrcodes->total(),
                'rows' => $qrcodes->items()
            ],
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function accept(Request $request) {
        $address = Address::find($request->get('id'));
        $address->status = 1;
        if ($address->save()) {
            return response()->json([
                'success' => true,
                'messages' => null
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'success' => false,
                'messages' => null
            ], Response::HTTP_OK);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Address $address) {
        $tokens = $address->getMyAddress($request);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => $tokens->total(),
                'rows' => $tokens->items()
            ],
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHistory(Request $request) {

        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:user_has_addresses,id',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $Assignment = Assignment_History::where('user_id', auth()->id())
            ->whereExists(function ($query) use ($request){
                $query->select("*")
                    ->from('assignments')
                    ->whereRaw('assignment_has_history.assignment_id = assignments.id')
                    ->whereExists(function ($query) use ($request){
                        $query->select("*")
                            ->from('tasks')
                            ->whereRaw('assignments.task_id = tasks.id')
                            ->whereExists(function ($query) use ($request){
                                $query->select("*")
                                    ->from('task_has_addresses')
                                    ->whereRaw('tasks.id = task_has_addresses.task_id')
                                    ->where('address_id', '=', $request->get('id'))
                            ;
                        })
                    ;
                })
            ;
        })->get();

        return response()->json([
            'success' => true,
            'actions' => AssignmentHistoryResource::collection($Assignment),
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActions(Request $request, Assignment_History $Assignment_History) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:user_has_addresses,id',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $tasks = Task::whereExists(
            function ($query) use ($request){
                $query->select("*")
                    ->from('task_has_addresses')
                    ->whereRaw('task_has_addresses.task_id = tasks.id')
                    ->where('task_has_addresses.address_id', '=', $request->get('id'))
                ;
            })->get();

//        $tasks = Datatables::of($tasks)
//            ->editColumn('date', function ($tasks) {
//                return $tasks->date;//Jalalian::fromDateTime($tasks->date)->toString();
//            })
////            ->editColumn('deadline', function ($tasks) {
////                Carbon::setLocale('fa');
////                $created = new Carbon($tasks->date);
////                $created->addMinutes($tasks->dealine);
////                return Carbon::now()->diffForHumans($created);
////            })
//            ->editColumn('status', function ($tasks) {
//                return $tasks->assignments[count($tasks->assignments) - 1]->last_status;
//            })
//            ->make(true);
        $tasks = TaskResource::collection($tasks);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => count($tasks),
                'rows' => $tasks
            ],
            'messages' => null
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Address $address) {
        $tokens = $address->getAddress($request);
        return response()->json(
            [
                'success' => true,
                'result' => [
                    'total' => $tokens->total(),
                    'rows' => $tokens->items()
                ],
                'messages' => null
            ], Response::HTTP_OK
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getListBani(Request $request, Address $address) {
        $Bani_Address = Bani_Address::where('user_has_addresses_id', $request->get('id'))->get();
        return response()->json(
            [
                'success' => true,
                'result' => [
                    'total' => count($Bani_Address),
                    'rows' => BaniCountersAddressResource::collection($Bani_Address)
                ],
                'messages' => null
            ], Response::HTTP_OK
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myCounters(Request $request, Address $address) {
        $tokens = $address->getMyCounters($request);
        return response()->json(
            [
                'success' => true,
                'result' => [
                    'total' => $tokens->total(),
                    'rows' => $tokens->items()
                ],
                'messages' => null
            ], Response::HTTP_OK
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleVerification(Request $request, Address $address) {

        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:selected_bellman_service,id',
                'verification_code' => 'required|numeric',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        if ($request->get('verification_code') == '123123') {
            $Bani_Address = Bani_Address::find($request->get('id'));
            $Bani_Address->is_verified = 1;
            if ($Bani_Address->save()) {
                return response()->json(
                    [
                        'success' => true,
                    ], Response::HTTP_OK
                );
            } else {
                return response()->json(
                    [
                        'success' => false,
                    ], Response::HTTP_BAD_REQUEST
                );
            }
        }
        return response()->json(
            [
                'success' => false,
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editCounter(Request $request, Address $address) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:selected_bellman_service,id',
                'counterType' => 'required|in:Owner,Renter,ColleagueManager',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $Bani_Address = Bani_Address::find($request->get('id'));
        $Bani_Address->associate_type = $request->get('counterType');
        if ($Bani_Address->save()) {
            return response()->json(
                [
                    'success' => true,
                ], Response::HTTP_OK
            );
        } else {
            return response()->json(
                [
                    'success' => false,
                ], Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(
            [
                'success' => false,
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function counters(Request $request, Address $address) {
        $Counters = Address_Counter::all();
        return response()->json(
            [
                'success' => true,
                'result' => [
                    'total' => count($Counters),
                    'rows' => AddressCountersResource::collection($Counters)
                ],
                'messages' => null
            ], Response::HTTP_OK
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectCounter(Request $request, Address $address) {

        $validator = Validator::make($request->all(),
            [
                'addressId' => 'required|exists:user_has_addresses,id',
                'associateType' => 'required|in:Owner,Renter,ColleagueManager',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $Bani_Address = Bani_Address::create([
            'user_has_addresses_id' => $request->get('addressId'),
            'bellman_id' => auth()->id(),
            'associate_type' => $request->get('associateType'),
        ]);
        if ($Bani_Address) {
            return response()->json(
                [
                    'message' => 'REQUEST_SUCCESSFULLY',
                    'success' => true
                ], Response::HTTP_OK
            );
        }
        return response()->json(
            [
                'message' => 'UNAUTHORIZD_ERROR'
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'province' => 'max:50',
                'city' => 'max:50',
                'address' => 'required|max:50',
                'region' => 'max:50',
                'address' => 'required|max:50',
                'row' => 'numeric',
                'column' => 'numeric',
                'geoPlace' => 'max:100',
            ],
            $this->customErrorMessages
        );

        if ($validator->fails()) {
            return response()->json(['success' => true, 'errors' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }

        $Address = Address::create([
            'user_id' => auth()->id(),
            'province' => $request->get('province'),
            'city' => $request->get('city'),
            'address' => $request->get('address'),
            'region' => $request->get('region'),
            'geo_place' => $request->get('geoPlace'),
            'column' => $request->get('column'),
            'row' => $request->get('row'),
        ]);
        return response()->json(['success' => true, 'address' => $Address->id, 'message' => 'COUNTER_ADDED_SUCCESSFULLY'], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendBani(Request $request) {
        $bani = json_decode($request->get('bani'));
        $Bani_Address = Bani_Address::updateOrCreate(
            ['user_has_addresses_id' => $request->get('address_id'), 'bellman_id' => $bani->node_id],
            ['associate_type' =>  $request->get('counterType'), 'is_verified' =>  1]
        );
        if($Bani_Address)
            return response()->json(['success' => true, 'message' => 'COUNTER_ADDED_SUCCESSFULLY'], Response::HTTP_OK);
        return response()->json(['success' => false], Response::HTTP_BAD_REQUEST);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function item($id, Address $address) {
        $address = $address->find($id);
        return response()->json(
            [
                'success' => true,
                'address' => $address,
                'messages' => null
            ], Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $Address = Address::where('id', $id)->delete();
        if ($Address)
            return response()->json(['success' => true, 'message' => 'COUNTER_ADDED_SUCCESSFULLY'], Response::HTTP_OK);

        return response()->json(
            [
                'message' => 'UNAUTHORIZD_ERROR'
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeCounter(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'addressId' => 'required|exists:selected_bellman_service,id',
            ]
        );
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $Bani_Address = Bani_Address::where('id', $request->get('addressId'))->delete();
        if ($Bani_Address) {
            return response()->json(
                [
                    'success' => true,
                ], Response::HTTP_OK
            );
        }
        return response()->json(
            [
                'success' => false,
            ], Response::HTTP_BAD_REQUEST
        );
    }

}
