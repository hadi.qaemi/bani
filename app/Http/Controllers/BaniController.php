<?php
/**
 * File PermissionController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;

use App\Http\Resources\AutoCompleteUserResource;
use App\Laravue\Models\User;
use App\Models\Address;
use App\Models\Assignment;
use App\Models\Assignment_History;
use App\Models\Bani;
use App\Models\History_Description;
use App\Models\Task_Address_Counter;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers
 */
class BaniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function searchBanis(Request $request)
    {

        $bani = Bani::with('user')
            ->whereHas('user', function($q) use ($request) {
                $q->where('name', 'like', '%'.$request->get('q').'%')
                    ->orWhere('family', 'like', '%'.$request->get('q').'%');
            });
        if($bani){
            return response()->json([
                'total_count' => $bani->count(),
                'incomplete_results' => true,
                'items' => AutoCompleteUserResource::collection($bani->get())
            ],Response::HTTP_OK);
        }else{
            return response()->json([
                'success' => false,
                'messages' => null
            ],Response::HTTP_OK);

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function accept(Request $request)
    {
        $bani = Bani::find($request->get('id'));
        $bani->validate = 1;
        if($bani->save()){
            return response()->json([
                'success' => true,
                'messages' => null
            ],Response::HTTP_OK);
        }else{
            return response()->json([
                'success' => false,
                'messages' => null
            ],Response::HTTP_OK);

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request, Bani $bani)
    {
        $bani = $bani->allBani($request);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => $bani->total(),
                'rows' => $bani->items()
            ],
            'messages' => null
        ],Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function history(Request $request, Bani $bani)
    {
        $Assignment_History = Task_Address_Counter::whereHas('files', function($q) {
            $q->whereNotNull('file_path');
        })->get();
        return response()->json([
            'success' => true,
            'result' => [
                'total' => count($Assignment_History),
                'rows' => \App\Http\Resources\AssignmentHistoryResource::collection($Assignment_History)
            ],
            'messages' => null
        ],Response::HTTP_OK);
    }

    public function submitContour(Request $request, Bani $bani)
    {
        if(trim($request->get('value')) != ''){
            Task_Address_Counter::find($request->get('id'))
                ->update([
                    'value' => $request->get('value')
                ]);

//            History_Description::updateOrCreate(
//                ['assignment_has_history_id' => $request->get('id')],
//                ['user_id' => auth()->id(), 'description' => $request->get('value')]
//            );
//
//            $Assignment_History = Assignment_History::find($request->get('id'));
//
//            Assignment::where('id', $Assignment_History->assignment_id)->update([
//                'last_status' => TaskController::DONE_TASK
//            ]);
        }else{
            Task_Address_Counter::find($request->get('id'))
                ->update([
                    'last_status' => TaskController::WAITEING_FOR_EDIT
                ]);
        }
        return response()->json([
            'success' => true,
            'messages' => null
        ],Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, Address $address)
    {
        $tokens = $address->getMyAddress($request);
        return response()->json([
            'success' => true,
            'result' => [
                'total' => $tokens->total(),
                'rows' => $tokens->items()
            ],
            'messages' => null
        ],Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, Address $address)
    {
        $tokens = $address->getAddress($request);
        return response()->json(
            [
                'success' => true,
                'result' => [
                    'total' => $tokens->total(),
                    'rows' => $tokens->items()
                ],
                'messages' => null
            ],Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'province' => 'required|max:50',
                'city' => 'required|max:50',
                'address' => 'required|max:50',
                'region' => 'required|max:50',
                'address' => 'required|max:50',
                'subscriptionNumber' => 'required|numeric',
                'geoPlace' => 'max:100',
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        Address::create([
            'user_id' => auth()->id(),
            'province' => $request->get('province'),
            'city' => $request->get('city'),
            'address' => $request->get('address'),
            'region' => $request->get('region'),
            'geo_place' => $request->get('geoPlace'),
            'counter' => $request->get('subscriptionNumber'),
        ]);
        return response()->json(['success' => true], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
