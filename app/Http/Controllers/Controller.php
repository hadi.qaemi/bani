<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Kavenegar\KavenegarApi;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $imageExtensions = [ "jpeg", "jpg", "png" ];
    public $maxImageSize = 20097152;


    public $customErrorMessages = [
        'required' => 'validators,:attribute,required',
        'unique' => 'validators,:attribute,unique',
        'match' => 'validators,:attribute,match',
        'same' => 'validators,:attribute,same',
        'email' => 'validators,:attribute,email',
        'min' => 'validators,:attribute,min::min,min',
        'max' => 'validators,:attribute,max::max,max',
        'exists' => 'validators,:attribute,notExists',
        'numeric' => 'validators,:attribute,numeric',
    ];

    public function sendCurl($url = '', $method ='GET'){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => env('ApiPort'),
            CURLOPT_URL => env('ApiUrl').$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }

        $curl = curl_init();
    }

    public function sendSms($receptor, $message, $sender =''){
        $sender = trim($sender) != '' ? : env('KAVENEGAR_SENDER');
        $KavenegarApi = new KavenegarApi(env('KAVENEGAR_KEY_ID'));
        if($KavenegarApi->Send($sender, $receptor, $message))
            return true;
        return false;
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    function myUploadfile($file, $directory = 'scan') {
        $file_name = $file['name'];
        $file_size = $file['size'];
        $file_tmp = $file['tmp_name'];
        $explode = explode('.',$file['name']);
        $file_ext = strtolower(end($explode));

        if(in_array($file_ext,$this->imageExtensions)=== false){
            $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
        }

        if($file_size > $this->maxImageSize){
            $errors[] = 'File size must be excately 20 MB';
        }

        if(empty($errors) == true){
            $file_name = "images/".$directory."/".time().'-'.mt_rand(100000000000, 1000000000000).'-'.$file_name;
            move_uploaded_file($file_tmp, public_path('/').$file_name);
            return $file_name;
        }
        return false;
    }
}
