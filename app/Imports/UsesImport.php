<?php

namespace App\Imports;

use App\Models\Uses;
use Maatwebsite\Excel\Concerns\ToModel;

class UsesImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        return new Uses([
            'number'     => $row[0],
            'uses'    => $row[1],
            'date' => $row[2],
        ]);
    }
}
