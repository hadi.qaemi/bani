<?php

use \App\Laravue\Faker;
use \App\Laravue\JsonResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'api'], function () {


    Route::group(['prefix' => 'address', 'as' => 'address.'], function(){
        Route::get('selected', 'AddressController@selected');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('{id}', 'AddressController@item');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('selected/{id}', 'AddressController@selectedAddress');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/verification', 'AddressController@sendVerification');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/owner', 'AddressController@addOwner');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/renter', 'AddressController@addRenter');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/support', 'AddressController@addSupport');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/qrCode', 'AddressController@addQrCode');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/setAttributeAddress', 'AddressController@setAttributeAddress');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/setAttributeCounter', 'AddressController@setAttributeCounter');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/counters', 'AddressController@addressCounters');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('selected/{id}/sendBani', 'AddressController@sendBani');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('selected/{id}/countersByPosition', 'AddressController@countersByPosition');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('selected/{id}/counters', 'AddressController@addressCounters');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('selected/{id}/log', 'AddressController@log');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::get('selected/{id}/bani', 'AddressController@getListBani');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
        Route::post('qrcodeSubscribtionCode', 'AddressController@qrcodeSubscribtionCode');//->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
    });

    Route::post('user/register', 'AuthController@registerMobile');
    Route::post('user/verify', 'AuthController@verificationMobile');
    Route::post('user/takeverify', 'AuthController@takeVerificationMobile');
    Route::post('user/login', 'AuthController@loginMobile');
    Route::post('user/forgetpassword', 'UserController@sendForgetPasswordMobile');
    Route::post('user/resetpassword', 'UserController@verifyForgetPasswordMobile');


    Route::post('auth/login', 'AuthController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');
    });

    Route::post('uses/transfer', 'UsesController@transfer')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE);
    Route::post('uses/upload', 'UsesController@upload')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE);
    Route::get('uses/all', 'UsesController@all')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE);
    Route::apiResource('users', 'UserController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE);
    Route::get('users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::put('users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::apiResource('roles', 'RoleController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::get('roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::apiResource('permissions', 'PermissionController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);

    Route::apiResource('tasks', 'TaskController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::get('myTasks', 'TaskController@myTasks')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::get('myTasksCount', 'TaskController@myTasksCount')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('removeAssignment', 'TaskController@removeAssignment')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('task/acceptAssignment', 'TaskController@acceptAssignment')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('sendAction', 'TaskController@sendAction')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('sendActionByMobile', 'TaskController@sendActionByMobile');
//    Route::apiResource('bani', 'BaniController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::apiResource('address', 'AddressController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_ADDRESS);
    Route::get('all', 'AddressController@all')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
    Route::post('accept', 'AddressController@accept')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
    Route::post('createQrcodes', 'AddressController@createQrcodes')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
    Route::post('sendQrcode', 'AddressController@sendQrcode')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);
    Route::post('listQrcodes', 'AddressController@listQrcodes')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONFIRM_ADDRESS);

    Route::post('address/counters', 'AddressController@counters')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/myCounters', 'AddressController@myCounters')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/removeCounter', 'AddressController@removeCounter')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/getActions', 'AddressController@getActions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::get('getAllActions', 'TaskController@getAllActions');
    Route::post('updateActionValue', 'TaskController@updateActionValue');
    Route::post('address/getHistory', 'AddressController@getHistory')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/handleVerification', 'AddressController@handleVerification')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/editCounter', 'AddressController@editCounter')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('address/selectCounter', 'AddressController@selectCounter')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::get('autoComplete/address', 'AutoCompleteController@address');
    Route::get('autoComplete/bani', 'AutoCompleteController@bani');
    Route::get('autoComplete/region', 'AutoCompleteController@region');

    Route::get('bani/list', 'BaniController@list')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::get('bani/history', 'BaniController@history')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('submitContour', 'BaniController@submitContour')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);
    Route::post('bani/accept', 'BaniController@accept')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_TASK);



    Route::post('registerBani', 'UserController@registerBani');

    Route::post('register', 'AuthController@register');
    Route::post('verification', 'AuthController@verification');
    Route::post('newVerificationCode', 'AuthController@newVerificationCode');
    Route::post('smsPwd', 'AuthController@smsPwd');

    Route::post('sendForgetPassword', 'UserController@sendForgetPassword');
    Route::post('verifyForgetPassword', 'UserController@verifyForgetPassword');
    Route::Post('user/registerUserBani', 'UserController@registerUserBani')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('activeUser', 'AuthController@activeUser');
    Route::post('editBani', 'AuthController@editBani');
    Route::post('deactiveUser', 'AuthController@deactiveUser');


    Route::get('transferCoin', 'WalletController@transferCoin')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::get('getListWalletTransfer', 'WalletController@getListWalletTransfer')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);

    Route::get('searchBanis', 'BaniController@searchBanis');
    Route::get('searchUsers', 'TokenController@searchUsers');
    Route::get('showWallet/{id}', 'WalletController@showWallet')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::get('fetchTokens/{id}', 'WalletController@fetchTokens')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('addToken', 'WalletController@addToken')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_TOKEN_ADD);
    Route::post('submitChargeUserAccountForm', 'WalletController@submitChargeUserAccountForm')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('submitChangeUserAccountToNetworkAccount', 'WalletController@submitChangeUserAccountToNetworkAccount')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('submitChangeNetworkAccountToUserAccount', 'WalletController@submitChangeNetworkAccountToUserAccount')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('showTokenWallet', 'WalletController@showTokenWallet')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);
    Route::post('TransmitToken', 'WalletController@TransmitToken')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MANAGE_USER);

});
