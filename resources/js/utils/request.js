import axios from 'axios';
import { Message } from 'element-ui';
import { getToken, setToken } from '@/utils/auth';
import i18n from '@/lang';

// Create axios instance
const service = axios.create({
  baseURL: process.env.MIX_BASE_API,
  timeout: 10000, // Request timeout
});

// Request intercepter
service.interceptors.request.use(
  config => {
    const token = getToken();
    if (token) {
      config.headers['Authorization'] = 'Bearer ' + getToken(); // Set JWT token
    }

    return config;
  },
  error => {
    // Do something with request error
    console.log(error); // for debug
    Promise.reject(error);
  }
);

// response pre-processing
service.interceptors.response.use(
  response => {
    if (response.headers.authorization) {
      setToken(response.headers.authorization);
      response.data.token = response.headers.authorization;
    }

    return response.data;
  },
  error => {
    let message = error.message;
    if (error.response.data && error.response.data.errors) {
      message = error.response.data.errors;
    } else if (error.response.data && error.response.data.errors) {
      message = error.response.data.errors;
    }

    var errors = '';
    if (typeof message === 'string'){
      errors = i18n.t(`errors.` + message);
    } else {
      for (const property in message) {
        var str = message[property].toString();
        var res = str.split(',');
        // console.log(`errors.` + res);
        if (res[0] === 'validators'){
          errors += i18n.t(`field.` + res[1]) + ' ' + i18n.t(`errors.` + res[2]) + '\n';
        } else {
          errors += i18n.t(`errors.` + message[property]) + '\n';
        }
      }
    }

    Message({
      message: errors,
      type: 'error',
      duration: 2 * 1000,
      position: 'bottom-left',
    });
    return Promise.reject(error);
  },
);

export default service;
