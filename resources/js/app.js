import Vue from 'vue';
import Cookies from 'js-cookie';
import ElementUI from 'element-ui';
import App from './views/App';
import store from './store';
import router from '@/router';
import i18n from './lang'; // Internationalization
import Notifications from 'vue-notification';
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';
import '@/icons'; // icon
import '@/permission'; // permission control

import * as filters from './filters'; // global filters
// if (store.getters.language === 'fa') {
//   require('../../public/css/app-rtl.css');
// } else {
//   require('../../public/css/app.css');
// }
import vSelect from 'vue-select';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
// Optionally install the BootstrapVue icon components plugin
Vue.component('v-select', vSelect);
Vue.component('date-picker', VuePersianDatetimePicker);
Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value),
});
Vue.use(Notifications);

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App),
});
