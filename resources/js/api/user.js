import request from '@/utils/request';
import Resource from '@/api/resource';

class UserResource extends Resource {
  constructor() {
    super('users');
  }

  permissions(id) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'get',
    });
  }

  updatePermission(id, permissions) {
    return request({
      url: '/' + this.uri + '/' + id + '/permissions',
      method: 'put',
      data: permissions,
    });
  }
  activeUser(resource) {
    return request({
      url: '/activeUser',
      method: 'post',
      data: resource,
    });
  }
  editBani(resource) {
    return request({
      url: '/editBani',
      method: 'post',
      data: resource,
    });
  }
  newVerificationCode(resource) {
    return request({
      url: '/newVerificationCode',
      method: 'post',
      data: resource,
    });
  }
  sendForgetPassword(resource) {
    return request({
      url: '/sendForgetPassword',
      method: 'post',
      data: resource,
    });
  }
  verifyForgetPassword(resource) {
    return request({
      url: '/verifyForgetPassword',
      method: 'post',
      data: resource,
    });
  }
  smsPwd(resource) {
    return request({
      url: '/smsPwd',
      method: 'post',
      data: resource,
    });
  }
  deactiveUser(resource) {
    return request({
      url: '/deactiveUser',
      method: 'post',
      data: resource,
    });
  }
}

export { UserResource as default };
