import request from '@/utils/request';

/**
 * Simple RESTful resource class
 */
class Task {
  constructor(uri) {
    this.uri = uri;
  }
  list(query) {
    return request({
      url: '/' + this.uri,
      method: 'get',
      params: query,
    });
  }
  myTasks(query) {
    return request({
      url: '/myTasks',
      method: 'get',
      params: query,
    });
  }
  get(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'get',
    });
  }
  show(id) {
    return request({
      url: '/showTask/' + id,
      method: 'get',
    });
  }
  removeAssignment(resource) {
    return request({
      url: '/removeAssignment',
      method: 'post',
      data: resource,
    });
  }
  acceptAssignment(resource) {
    return request({
      url: '/task/acceptAssignment',
      method: 'post',
      data: resource,
    });
  }
  sendAction(resource) {
    return request({
      url: '/sendAction',
      method: 'post',
      data: resource,
    });
  }
  registerBani(resource) {
    return request({
      url: '/registerBani',
      method: 'post',
      data: resource,
    });
  }
  registerCreateBani(resource) {
    return request({
      url: '/user/registerUserBani',
      method: 'post',
      data: resource,
    });
  }
  autoCompleteRegion(resource) {
    return request({
      url: '/autoComplete/region',
      method: 'post',
      data: resource,
    });
  }
  store(resource) {
    return request({
      url: '/' + this.uri,
      method: 'post',
      data: resource,
    });
  }
  update(id, resource) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'put',
      data: resource,
    });
  }
  destroy(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'delete',
    });
  }
}

export { Task as default };
