import request from '@/utils/request';

/**
 * Simple RESTful resource class
 */
class Buy {
  constructor(uri) {
    this.uri = uri;
  }
  list(query) {
    return request({
      url: '/' + this.uri,
      method: 'get',
      params: query,
    });
  }
  mine(query) {
    return request({
      url: '/mine',
      params: query,
      method: 'get',
    });
  }
  show(id) {
    return request({
      url: '/showBuy/' + id,
      method: 'get',
    });
  }
  confirmToken(resource) {
    return request({
      url: '/confirmToken',
      data: resource,
      method: 'post',
    });
  }
  deleteToken(resource) {
    return request({
      url: '/deleteToken',
      data: resource,
      method: 'post',
    });
  }
}

export { Buy as default };
