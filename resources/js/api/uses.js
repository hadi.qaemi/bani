import request from '@/utils/request';
import Resource from '@/api/resource';

class UsesResource extends Resource {
  constructor() {
    super('uses');
  }

  upload(resource) {
    return request({
      url: '/uses/upload',
      method: 'post',
      data: resource,
    });
  }

  transfer(resource) {
    return request({
      url: '/uses/transfer',
      method: 'post',
      data: resource,
    });
  }

  all(query) {
    return request({
      url: '/uses/all',
      method: 'get',
      params: query,
    });
  }
}

export { UsesResource as default };
