import request from '@/utils/request';

/**
 * Simple RESTful resource class
 */
class Bani {
  constructor(uri) {
    this.uri = uri;
  }
  list(query) {
    return request({
      url: '/bani/list',
      method: 'get',
      params: query,
    });
  }
  history(query) {
    return request({
      url: '/bani/history',
      method: 'get',
      params: query,
    });
  }
  submitContour(resource) {
    return request({
      url: '/submitContour',
      method: 'post',
      data: resource,
    });
  }
  accept(resource) {
    return request({
      url: '/' + this.uri + '/accept',
      method: 'post',
      data: resource,
    });
  }
  createQrcodes(resource) {
    return request({
      url: '/createQrcodes',
      method: 'post',
      data: resource,
    });
  }
  listQrcodes(resource) {
    return request({
      url: '/listQrcodes',
      method: 'post',
      data: resource,
    });
  }
  sendQrcode(resource) {
    return request({
      url: '/sendQrcode',
      method: 'post',
      data: resource,
    });
  }
  get(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'get',
    });
  }
  store(resource) {
    return request({
      url: '/' + this.uri,
      method: 'post',
      data: resource,
    });
  }
  update(id, resource) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'put',
      data: resource,
    });
  }
  destroy(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'delete',
    });
  }
}

export { Bani as default };
