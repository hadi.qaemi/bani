import request from '@/utils/request';

/**
 * Simple RESTful resource class
 */
class Address {
  constructor(uri) {
    this.uri = uri;
  }
  list(query) {
    return request({
      url: '/' + this.uri,
      method: 'get',
      params: query,
    });
  }
  all(query) {
    return request({
      url: '/all',
      method: 'get',
      params: query,
    });
  }
  getListBani(query) {
    return request({
      url: '/' + this.uri + '/selected/' + query.id + '/bani',
      method: 'get',
      params: query,
    });
  }
  getAddressCounter(query) {
    return request({
      url: '/' + this.uri + '/selected/' + query.id + '/countersByPosition',
      method: 'get',
      params: query,
    });
  }
  getAddress(query) {
    return request({
      url: '/' + this.uri + '/' + query.id,
      method: 'get',
      params: query,
    });
  }
  counters(query) {
    return request({
      url: '/' + this.uri + '/counters',
      method: 'post',
      params: query,
    });
  }
  setAttributeAddress(query) {
    return request({
      url: '/' + this.uri + '/selected/' + query.id + '/setAttributeAddress',
      method: 'post',
      params: query,
    });
  }
  sendBani(query) {
    return request({
      url: '/' + this.uri + '/selected/' + query.address_id + '/sendBani',
      method: 'post',
      params: query,
    });
  }
  setAttributeCounter(query) {
    return request({
      url: '/' + this.uri + '/selected/' + query.id + '/setAttributeCounter',
      method: 'post',
      params: query,
    });
  }
  qrcodeSubscribtionCode(query) {
    return request({
      url: '/' + this.uri + '/qrcodeSubscribtionCode',
      method: 'post',
      params: query,
    });
  }
  myCounters(query) {
    return request({
      url: '/' + this.uri + '/myCounters',
      method: 'post',
      params: query,
    });
  }
  removeCounter(query) {
    return request({
      url: '/' + this.uri + '/removeCounter',
      method: 'post',
      params: query,
    });
  }
  getActions(query) {
    return request({
      url: '/' + this.uri + '/getActions',
      method: 'post',
      params: query,
    });
  }
  getHistory(query) {
    return request({
      url: '/' + this.uri + '/getHistory',
      method: 'post',
      params: query,
    });
  }
  handleVerification(query) {
    return request({
      url: '/' + this.uri + '/handleVerification',
      method: 'post',
      params: query,
    });
  }
  editCounter(query) {
    return request({
      url: '/' + this.uri + '/editCounter',
      method: 'post',
      params: query,
    });
  }
  selectCounter(resource) {
    return request({
      url: '/' + this.uri + '/selectCounter',
      method: 'post',
      data: resource,
    });
  }

  get(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'get',
    });
  }
  store(resource) {
    return request({
      url: '/' + this.uri,
      method: 'post',
      data: resource,
    });
  }
  accept(resource) {
    return request({
      url: '/accept',
      method: 'post',
      data: resource,
    });
  }
  update(id, resource) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'put',
      data: resource,
    });
  }
  destroy(id) {
    return request({
      url: '/' + this.uri + '/' + id,
      method: 'delete',
    });
  }
}

export { Address as default };
