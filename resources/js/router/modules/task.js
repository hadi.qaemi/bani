import Layout from '@/layout';

const taskRoutes = {
  path: '/task',
  component: Layout,
  redirect: '/permission/index',
  alwaysShow: true, // will always show the root menu
  meta: {
    title: 'task',
    icon: 'dollar',
    permissions: ['view menu task'],
  },
  children: [
    {
      path: 'tasks',
      component: () => import('@/views/tasks/List'),
      name: 'listTask',
      meta: {
        title: 'listTask',
      },
    },
    {
      path: 'show/:id(WT\-.+)',
      component: () => import('@/views/tasks/Show'),
      name: 'showTask',
      meta: { title: 'showTask', noCache: true, permissions: ['manage task'] },
      hidden: true,
    },
    {
      path: 'setting/:id(WT\-.+)',
      component: () => import('@/views/tasks/Setting'),
      name: 'showTask',
      meta: { title: 'showTask', noCache: true, permissions: ['manage task'] },
      hidden: true,
    },
    {
      path: 'transmit-token/:id(.+)',
      component: () => import('@/views/tasks/TransmitToken'),
      name: 'showTask',
      meta: { title: 'showTask', noCache: true, permissions: ['manage task'] },
      hidden: true,
    },
  ],
};

export default taskRoutes;
