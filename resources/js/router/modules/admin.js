/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const adminRoutes = {
  path: '/administrator',
  component: Layout,
  redirect: '/administrator/users',
  name: 'Administrator',
  alwaysShow: true,
  meta: {
    title: 'administrator',
    icon: 'admin',
    permissions: ['view menu administrator'],
  },
  children: [
    /** User managements */
    {
      path: 'users/edit/:id(\\d+)',
      component: () => import('@/views/users/Profile'),
      name: 'UserProfile',
      meta: { title: 'userProfile', noCache: true, permissions: ['manage user'] },
      hidden: true,
    },
    {
      path: 'bani',
      component: () => import('@/views/banis/Create'),
      name: 'AdminBani',
      meta: { title: 'baniCreate', icon: 'user', noCache: false },
    },
    {
      path: 'createTask',
      component: () => import('@/views/tasks/Create'),
      name: 'formTask',
      meta: {
        title: 'createTask', icon: 'user',
      },
    },
    {
      path: 'users',
      component: () => import('@/views/users/List'),
      name: 'UserList',
      meta: { title: 'users', icon: 'user', permissions: ['manage user'] },
    },
    /** Role and permission */
    {
      path: 'roles',
      component: () => import('@/views/role-permission/List'),
      name: 'RoleList',
      meta: { title: 'rolePermission', icon: 'role', permissions: ['manage permission'] },
    },
    {
      path: 'addresses',
      component: () => import('@/views/addresses/List'),
      name: 'address',
      meta: { title: 'address', icon: 'role', permissions: ['confirm address'] },
    },
    {
      path: 'banis',
      component: () => import('@/views/banis/List'),
      name: 'bani',
      meta: { title: 'bani', icon: 'role', permissions: ['confirm bani'] },
    },
    {
      path: 'history',
      component: () => import('@/views/banis/History'),
      name: 'history',
      meta: { title: 'history', icon: 'role', permissions: ['confirm bani'] },
    },
    {
      path: 'qr-codes',
      component: () => import('@/views/banis/Qrcodes'),
      name: 'qrcodes',
      meta: { title: 'qrcodes', icon: 'role', permissions: ['confirm bani'] },
    },
    {
      path: 'qr-codes-list',
      component: () => import('@/views/banis/QrcodesList'),
      name: 'qrcodesList',
      meta: { title: 'qrcodesList', icon: 'role', permissions: ['confirm bani'] },
    },
  ],
};

export default adminRoutes;
