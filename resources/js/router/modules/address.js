import Layout from '@/layout';

const addressRoutes = {
  path: '/address',
  component: Layout,
  redirect: '/permission/index',
  alwaysShow: true, // will always show the root menu
  meta: {
    title: 'address',
    icon: 'example',
    permissions: ['view menu address'],
  },
  children: [
    {
      path: 'addAddresss',
      component: () => import('@/views/addresses/Create'),
      name: 'addAddresss',
      meta: {
        title: 'addAddress',
      },
    },
    // {
    //   path: 'addAddressUser',
    //   component: () => import('@/views/addresses/CreateAddressUser'),
    //   name: 'addAddressUser',
    //   meta: {
    //     title: 'addAddressUser',
    //   },
    // },
    {
      path: 'myAddress',
      component: () => import('@/views/addresses/Mine'),
      name: 'myAddress',
      meta: {
        title: 'myAddress',
      },
    },
    {
      path: ':id(\\d+)/counters',
      component: () => import('@/views/addresses/AddressCounters'),
      name: 'addressCounters',
      meta: { title: 'addressCounters', noCache: true },
      hidden: true,
    },
    // {
    //   path: 'my-addresss',
    //   component: () => import('@/views/addresss/mine'),
    //   name: 'myAddresss',
    //   meta: {
    //     title: 'myAddresss',
    //     // if do not set roles neither permissions, means: this page does not require permission
    //   },
    // },
  ],
};

export default addressRoutes;
