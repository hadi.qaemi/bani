import Layout from '@/layout';

const baniRoutes = {
  path: '/bani',
  component: Layout,
  redirect: '/permission/index',
  alwaysShow: true, // will always show the root menu
  meta: {
    title: 'bani',
    icon: 'dollar',
    permissions: ['view menu bani'],
  },
  children: [
    {
      path: 'banis',
      component: () => import('@/views/banis/Create'),
      name: 'listBani',
      meta: {
        title: 'listBani',
      },
    },
    {
      path: 'create',
      component: () => import('@/views/banis/Create'),
      name: 'formBani',
      meta: {
        title: 'createBani',
      },
    },
  ],
};

export default baniRoutes;
