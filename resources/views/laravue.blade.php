<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bani</title>
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/favicon.png">
    <meta name="theme-color" content="#ffffff">
    <link href="/css/app-design.css" type="text/css" rel="stylesheet" />
    <link href="/css/app-rtl.css" type="text/css" rel="stylesheet" />
    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="/css/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <div id="app" dir="rtl">
        <app></app>
    </div>

    <script src=/static/tinymce4.7.5/tinymce.min.js></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
